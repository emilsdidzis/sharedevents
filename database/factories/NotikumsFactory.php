<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Notikums>
 */
class NotikumsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $sakums=fake()->dateTimeBetween('now', '+1 year');
        $beigas=fake()->dateTimeInInterval($sakums, '+2 days');
        return [
            'virsraksts' =>fake()->text(100),
            'saturs'=>fake()->text(7500),
            'sakums' =>$sakums,
            'beigas' =>$beigas,
            'var_pieteikties' =>rand(0,1),
        ];
    }
}
