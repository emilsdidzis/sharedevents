<?php

namespace Database\Seeders;

use App\Models\Komentars;
use Illuminate\Database\Seeder;

class KomentaruSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $id=1;
        for ($i=1;$i<=10;$i++) {
            for ($j=$i;$j<=10;$j++) {
            Komentars::create(['id' =>$id, 'notikums_id' => $i, 'autora_id' => $j, 'saturs' => 'Komentara saturs i: '.$i.'j: '.$j]);
            $id++;
            }
        }
    }
}
