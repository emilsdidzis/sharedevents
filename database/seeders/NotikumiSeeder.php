<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Notikums;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NotikumiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i=1;$i<=10;$i++) {
            $sakums=fake()->dateTimeBetween('now', '+1 year');
            $beigas=fake()->dateTimeInInterval($sakums, '+2 days');
            Notikums::create(['id' =>$i, 
            'virsraksts' => $i.' '.$faker->sentence(3, true),
            'saturs' => $faker->sentence($i==2 ? 300 : 30, true),
            'autora_id' => $i, 
            'sakums' =>$sakums, 
            'beigas' =>$beigas,
            'var_pieteikties' => 1]);
        }
        $menesaSakums=Carbon::create(null,null,1, 0);
        for ($i=1;$i<=30;$i++)
        {
            $sakums=fake()->dateTimeBetween($menesaSakums->toDateTime(), '+2 months');
            $beigas=fake()->dateTimeInInterval($sakums, '+7 days');
            $notikums = Notikums::create([
            'virsraksts' => 'Virsraksts '.$i,
            'saturs' => $faker->sentence($i==2 ? 300 : 30, true),
            'autora_id' => 1, 
            'sakums' =>$sakums, 
            'beigas' =>$beigas ,
            'var_pieteikties' => 1]);
            DB::table('notikuma_lietotaji')->insert(['users_id' => 1, 'notikums_id' => $notikums->id, 'pieteicies' => $i%2==0]);
        }
    }
}
