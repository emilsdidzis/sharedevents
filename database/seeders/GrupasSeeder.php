<?php

namespace Database\Seeders;

use App\Models\Grupa;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GrupasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($i=1;$i<=10;$i++) {
            Grupa::create(['id' =>$i, 'nosaukums' => 'Grupa '.$i, 'autora_id' => $i]);
            for ($j=$i;$j<=10;$j++) {
                DB::table('notikuma_grupas')->insert(['grupa_id' => $i, 'notikums_id' => $j]);

            }
        }
    }
}
