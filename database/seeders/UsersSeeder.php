<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory(10)->create();
        $mansUser = User::findOrFail(1);
        $mansUser->social_provider_id=env('GMAIL_TOKEN_ID');
        $mansUser->name = 'Emīls Google';
        $mansUser->social_provider='google';
        $mansUser->is_admin=1;
        $mansUser->email='bokspro@gmail.com';
        $mansUser->sanemt_pazinojumus=1;
        $mansUser2 = User::findOrFail(2);
        $mansUser2->social_provider_id=env('FACEBOOK_TOKEN_ID');
        $mansUser2->social_provider='facebook';
        $mansUser2->name = 'Emīls Facebook';
        $mansUser2->email = 'didzisemils1@inbox.lv';
        $mansUser->save();
        $mansUser2->save();
        for ($i=1;$i<=10;$i++) {
            for ($j=$i;$j<=10;$j++) {
                DB::table('piederiba_grupai')->insert(['users_id' => $i, 'grupa_id' => $j]);
                DB::table('notikuma_lietotaji')->insert(['users_id' => $i, 'notikums_id' => $j, 'pieteicies' => $j%2==0]);
                //DB::table('pieteikusies_notikumam')->insert(['users_id' => $i, 'notikums_id' => $j]);
            }
        }
        DB::table('bloketie_lietotaji')->insert(['bloketajs' =>1, 'blokejamais'=>2]);
        DB::table('bloketas_grupas')->insert(['bloketajs' =>1, 'bloketa_grupa'=>2]);
        
    }
}
