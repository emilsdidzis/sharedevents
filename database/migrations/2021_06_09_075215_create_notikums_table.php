<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotikumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notikums', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('virsraksts', 100);
            $table->string('saturs', 7500)->nullable();
            $table->unsignedBigInteger('autora_id')->nullable();
            $table->foreign('autora_id')->references('id')->on('users')->nullable();
            $table->unsignedBigInteger('webex_meeting_id')->nullable();
            $table->foreign('webex_meeting_id')->references('id')->on('webex_meeting')->nullable();
            $table->dateTime('sakums');
            $table->dateTime('beigas');
            $table->boolean('var_pieteikties')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notikums');
    }
}
