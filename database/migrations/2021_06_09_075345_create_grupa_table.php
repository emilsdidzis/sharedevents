<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGrupaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grupa', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('nosaukums', 50);
            $table->unsignedBigInteger('autora_id')->nullable();
            $table->foreign('autora_id')->references('id')->on('users')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grupa');
    }
}
