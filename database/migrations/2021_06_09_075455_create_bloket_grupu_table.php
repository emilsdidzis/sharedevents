<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBloketGrupuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bloketas_grupas', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger('bloketajs')->nullable();
            $table->unsignedBigInteger('bloketa_grupa')->nullable();
            $table->foreign('bloketajs')->references('id')->on('users')->nullable();
            $table->foreign('bloketa_grupa')->references('id')->on('grupa')->nullable();
            $table->unique(['bloketajs', 'bloketa_grupa']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bloketas_grupas');
    }
}
