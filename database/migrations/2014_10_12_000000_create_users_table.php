<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('social_provider_id', 51)->nullable();
            $table->string('social_provider', 30)->nullable();
            $table->string('name', 255);
            $table->string('email', 256); // RFC 5321
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('avatar_url', 201)->nullable();
            $table->integer('is_admin')->default(0);
            $table->integer('pazinot_pirms')->default(60);
            $table->boolean('sanemt_pazinojumus')->default(false);
            $table->string('valoda', 2)->default('lv');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
