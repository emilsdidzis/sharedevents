<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webex_user_tokens', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('access_token');
            $table->dateTime('expires');
            $table->string('refresh_token');
            $table->dateTime('refresh_token_expires');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webex_user_tokens');
    }
};
