<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotikumaGrupasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notikuma_grupas', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('notikums_id')->constrained();
            $table->unsignedBigInteger('grupa_id');
            $table->foreign('grupa_id')->references('id')->on('grupa');
            $table->unique(['notikums_id', 'grupa_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notikuma_grupas');
    }
}
