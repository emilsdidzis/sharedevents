<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBloketLietotajuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bloketie_lietotaji', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger('bloketajs')->nullable();
            $table->unsignedBigInteger('blokejamais')->nullable();
            $table->foreign('bloketajs')->references('id')->on('users');
            $table->foreign('blokejamais')->references('id')->on('users');
            $table->unique(['bloketajs', 'blokejamais']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bloketie_lietotaji');
    }
}
