<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePiederibaGrupaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('piederiba_grupai', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('users_id')->constrained()->nullable();
            $table->unsignedBigInteger('grupa_id')->nullable();
            $table->foreign('grupa_id')->references('id')->on('grupa');
            $table->unique(['users_id', 'grupa_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('piederiba_grupai');
    }
}
