<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotikumaLietotajiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notikuma_lietotaji', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('users_id')->constrained()->nullable();
            $table->foreignId('notikums_id')->constrained()->nullable();
            $table->boolean('pieteicies')->default(false);
            $table->boolean('pazinots')->default(false);
            $table->unique(['users_id', 'notikums_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notikuma_lietotaji');
    }
}
