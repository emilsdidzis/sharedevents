<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Grupa
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $nosaukums
 * @property int|null $autora_id
 * @property-read \App\Models\User|null $Autors
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $BloketieUsers
 * @property-read int|null $bloketie_users_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Notikums[] $GrupasNotikumi
 * @property-read int|null $grupas_notikumi_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $GrupasUsers
 * @property-read int|null $grupas_users_count
 * @method static \Database\Factories\GrupaFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Grupa newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Grupa newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Grupa query()
 * @method static \Illuminate\Database\Eloquent\Builder|Grupa whereAutoraId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Grupa whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Grupa whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Grupa whereNosaukums($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Grupa whereUpdatedAt($value)
 */
	class Grupa extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Komentars
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $notikums_id
 * @property int|null $autora_id
 * @property string $saturs
 * @property-read \App\Models\User|null $Autors
 * @property-read \App\Models\Notikums $Notikums
 * @method static \Database\Factories\KomentarsFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Komentars newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Komentars newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Komentars query()
 * @method static \Illuminate\Database\Eloquent\Builder|Komentars whereAutoraId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Komentars whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Komentars whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Komentars whereNotikumsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Komentars whereSaturs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Komentars whereUpdatedAt($value)
 */
	class Komentars extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Notikums
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $virsraksts
 * @property string|null $saturs
 * @property int|null $autora_id
 * @property int|null $webex_meeting_id
 * @property string $sakums
 * @property string $beigas
 * @property int $var_pieteikties
 * @property-read \App\Models\User|null $Autors
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Komentars[] $Komentari
 * @property-read int|null $komentari_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Grupa[] $NotikumaGrupas
 * @property-read int|null $notikuma_grupas_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $NotikumaUsers
 * @property-read int|null $notikuma_users_count
 * @property-read \App\Models\WebexMeeting|null $WebexMeeting
 * @method static \Database\Factories\NotikumsFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Notikums newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Notikums newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Notikums query()
 * @method static \Illuminate\Database\Eloquent\Builder|Notikums whereAutoraId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notikums whereBeigas($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notikums whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notikums whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notikums whereSakums($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notikums whereSaturs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notikums whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notikums whereVarPieteikties($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notikums whereVirsraksts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notikums whereWebexMeetingId($value)
 */
	class Notikums extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property string|null $social_provider_id
 * @property string|null $social_provider
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string|null $password
 * @property string|null $avatar_url
 * @property int $is_admin
 * @property int $pazinot_pirms
 * @property int $sanemt_pazinojumus
 * @property string $valoda
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Grupa[] $BloketasGrupas
 * @property-read int|null $bloketas_grupas_count
 * @property-read \Illuminate\Database\Eloquent\Collection|User[] $BloketieUsers
 * @property-read int|null $bloketie_users_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Komentars[] $Komentari
 * @property-read int|null $komentari_count
 * @property-read \Illuminate\Database\Eloquent\Collection|User[] $NoblokejusieUsers
 * @property-read int|null $noblokejusie_users_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Grupa[] $UserIzveidotasGrupas
 * @property-read int|null $user_izveidotas_grupas_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Notikums[] $UserIzveidotieNotikumi
 * @property-read int|null $user_izveidotie_notikumi_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Grupa[] $UseraGrupas
 * @property-read int|null $usera_grupas_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Notikums[] $UseraNotikumi
 * @property-read int|null $usera_notikumi_count
 * @property-read \App\Models\WebexUserTokens|null $WebexTokens
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Database\Factories\UserFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAvatarUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereIsAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePazinotPirms($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereSanemtPazinojumus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereSocialProvider($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereSocialProviderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereValoda($value)
 */
	class User extends \Eloquent implements \Illuminate\Contracts\Auth\MustVerifyEmail {}
}

namespace App\Models{
/**
 * App\Models\WebexMeeting
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $meeting_id
 * @property string $join_url
 * @property-read \App\Models\Notikums|null $Notikums
 * @method static \Illuminate\Database\Eloquent\Builder|WebexMeeting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|WebexMeeting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|WebexMeeting query()
 * @method static \Illuminate\Database\Eloquent\Builder|WebexMeeting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WebexMeeting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WebexMeeting whereJoinUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WebexMeeting whereMeetingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WebexMeeting whereUpdatedAt($value)
 */
	class WebexMeeting extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\WebexUserTokens
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $access_token
 * @property string $expires
 * @property string $refresh_token
 * @property string $refresh_token_expires
 * @property int $user_id
 * @property-read WebexUserTokens|null $User
 * @method static \Illuminate\Database\Eloquent\Builder|WebexUserTokens newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|WebexUserTokens newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|WebexUserTokens query()
 * @method static \Illuminate\Database\Eloquent\Builder|WebexUserTokens whereAccessToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WebexUserTokens whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WebexUserTokens whereExpires($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WebexUserTokens whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WebexUserTokens whereRefreshToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WebexUserTokens whereRefreshTokenExpires($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WebexUserTokens whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WebexUserTokens whereUserId($value)
 */
	class WebexUserTokens extends \Eloquent {}
}

