<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    public $ajaxHeader = ['X-Requested-With'=>'XMLHttpRequest'];
    public function SimulateLoggedInUser()
    {

        $user = User::findOrFail(User::factory()->create()->id);
        $this->actingAs($user);
        return $user;
    }
    public function SimulateLoggedInAdmin()
    {

        $user = User::findOrFail(User::factory()->create([
            'is_admin'=>1
        ])->id);
        $this->actingAs($user);
        return $user;
    }
    public function view_can_be_rendered_by_user($uri)
    {
        $user = $this->SimulateLoggedInUser();
        $response = $this->get($uri);
        $response->assertStatus(200);
    }
    public function view_can_be_rendered_by_guest($uri)
    {
        $response = $this->get($uri);
        $response->assertStatus(200);
    }
}
