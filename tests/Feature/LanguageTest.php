<?php

namespace Tests\Feature;

namespace App\Http\Controllers;

use Tests\TestCase;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LanguageTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_guests_can_set_language()
    {
        $this->get('/login');
        $response = $this->post('/language/lv');

        $response->assertRedirect('/login');
        $this->assertEquals('lv', session('locale'));
        $response =$this->post('/language/en');
        $response->assertRedirect('/login');
        $this->assertEquals('en', session('locale'));
    }
    public function test_users_can_not_set_other_languages()
    {
        $response = $this->post('/language/it');
        $response->assertForbidden();
    }
    public function test_language_is_saved_for_user()
    {
        $user = User::factory()->create();
        $this->actingAs($user);
        $this->post('/language/lv');
        $this->assertEquals('lv', session('locale'));
        $this->assertEquals('lv', $user->valoda);

    }
}
