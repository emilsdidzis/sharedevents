<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Grupa;
use App\Models\Notikums;
use App\Models\Komentars;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EventTest extends TestCase
{
    private function DateTimeStringFromString($dateTimeString)
    {
        return date(config('app.timeformat'),strToTime($dateTimeString));
    }
    private function getFakeInterval()
    {
        $sakums=fake()->dateTimeBetween('now', '+1 year');
        $beigas=fake()->dateTimeInInterval($sakums, '+4 hours');
        return [
            'sakums' =>$sakums->format('c'),
            'beigas' =>$beigas->format('c'),
        ];
    }
    private function getRandomNotikumaVertibas()
    {
        $intervals=$this->getFakeInterval();
        return array(
            'virsraksts'=>fake()->text(100),
            'saturs'=>fake()->text(7500),
            'sakums'=>$intervals['sakums'],
            'beigas'=>$intervals['beigas'],
            'var_pieteikties'=>rand(0,1)==0 ? null : 1,
        );
    }
    private function getNotikumaVertibasApaksejasRobezas()
    {
        $intervals=$this->getFakeInterval();
        return array(
            'virsraksts'=>'12',
            'saturs'=>'',
            'sakums'=>'sis-nav-datums',
            'beigas'=>'sis-nav-datums',
            'var_pieteikties'=>rand(0,1)==0 ? null : 1,
        );
    }
    private function getNotikumaVertibasAugsejasRobezas()
    {
        $intervals=$this->getFakeInterval();
        return array(
            'virsraksts'=>fake()->realTextBetween(102, 150),
            'saturs'=>'',
            'sakums'=>'sis-nav-datums',
            'beigas'=>'sis-nav-datums',
            'var_pieteikties'=>rand(0,1)==0 ? null : 1,
        );
    }
    public function test_future_event_list_view_can_be_rendered()
    {
        $this->SimulateLoggedInUser();
        $response=$this->get(route('home'));
        $response->assertOk();
        $response->assertViewIs('event.events');
    }
    public function test_event_list_view_can_be_rendered()
    {
        $this->SimulateLoggedInUser();
        $response=$this->get(route('event.all'));
        $response->assertOk();
        $response->assertViewIs('event.events');
    }

    public function test_user_can_edit_existing_event()
    {
        $user=$this->SimulateLoggedInUser();
        $notikums=Notikums::factory()->create(['autora_id'=>$user->id]);

        $editojamasVertibas=$this->getRandomNotikumaVertibas();
        $response=$this->post(route('event.store', $notikums->id), $editojamasVertibas);
        $response->assertRedirect(route('event.view', $notikums->id));
        $notikums->refresh();
        $this->assertTrue($notikums->virsraksts == $editojamasVertibas['virsraksts']);
        $this->assertTrue($notikums->saturs == $editojamasVertibas['saturs']);
        $this->assertTrue($notikums->sakums == $this->DateTimeStringFromString($editojamasVertibas['sakums']));
        $this->assertTrue($notikums->beigas == $this->DateTimeStringFromString($editojamasVertibas['beigas']));
        $this->assertTrue($notikums->var_pieteikties == $editojamasVertibas['var_pieteikties'] ?? 0);
    }
    public function test_user_can_not_edit_existing_event_with_bad_values()
    {
        $user=$this->SimulateLoggedInUser();
        $notikums=Notikums::factory()->create(['autora_id'=>$user->id]);

        $editojamasVertibas=$this->getNotikumaVertibasApaksejasRobezas();
        $response=$this->post(route('event.store', $notikums->id), $editojamasVertibas);
        $response->assertSessionHasErrors();
        $editojamasVertibas=$this->getNotikumaVertibasAugsejasRobezas();
        $response=$this->post(route('event.store', $notikums->id), $editojamasVertibas);
        $response->assertSessionHasErrors();
        
    }
    public function test_user_can_create_event()
    {
        $user=$this->SimulateLoggedInUser();
        $editojamasVertibas=$this->getRandomNotikumaVertibas();
        $response=$this->post(route('event.store', -1), $editojamasVertibas);
        $notikums=Notikums::orderBy('id', 'desc')->first();
        $response->assertRedirect(route('event.view', $notikums->id));
        $this->assertTrue($notikums->virsraksts == $editojamasVertibas['virsraksts']);
        $this->assertTrue($notikums->saturs == $editojamasVertibas['saturs']);
        $this->assertTrue($notikums->sakums == $this->DateTimeStringFromString($editojamasVertibas['sakums']));
        $this->assertTrue($notikums->beigas == $this->DateTimeStringFromString($editojamasVertibas['beigas']));
        $this->assertTrue($notikums->var_pieteikties == $editojamasVertibas['var_pieteikties'] ?? 0);
        $this->assertTrue($notikums->autora_id == $user->id);

    }
    public function test_user_can_not_edit_another_user_events()
    {
        $this->SimulateLoggedInUser();
        $autors=User::factory()->create();
        $notikums=Notikums::factory()->create(['autora_id'=>$autors->id]);
        $response=$this->post(route('event.store', $notikums->id), $this->getRandomNotikumaVertibas());
        $response->assertForbidden();

    }
    public function test_admin_can_edit_another_user_events()
    {

        $this->SimulateLoggedInAdmin();
        $autors=User::factory()->create();
        $notikums=Notikums::factory()->create(['autora_id'=>$autors->id]);
        $notikumaVertibas=$this->getRandomNotikumaVertibas();
        $response=$this->post(route('event.store', $notikums->id), $notikumaVertibas);
        $response->assertRedirect(route('event.view', $notikums->id));
        $notikums->refresh();
        $this->assertTrue($notikums->virsraksts == $notikumaVertibas['virsraksts']);
        $this->assertTrue($notikums->saturs == $notikumaVertibas['saturs']);
        $this->assertTrue($notikums->sakums == $this->DateTimeStringFromString($notikumaVertibas['sakums']));
        $this->assertTrue($notikums->beigas == $this->DateTimeStringFromString($notikumaVertibas['beigas']));
        $this->assertTrue($notikums->var_pieteikties == $notikumaVertibas['var_pieteikties'] ?? 0);
        $this->assertTrue($notikums->autora_id == $autors->id);
    }
    public function test_user_can_not_join_event_if_not_invited()
    {
        $user=$this->SimulateLoggedInUser();
        $autors=User::factory()->create();
        $notikums=Notikums::factory()->create(['autora_id'=>$autors->id]);
        $response=$this->post(route('event.join'), [
            'id' => $notikums->id
        ]);
        $response->assertJsonStructure(['Error']);
    }
    public function test_user_can_join_and_unjoin_event()
    {
        $user=$this->SimulateLoggedInUser();
        $autors=User::factory()->create();
        $notikums=Notikums::factory()->create(['autora_id'=>$autors->id]);
        $notikums->NotikumaUsers()->attach($user);
        $response=$this->post(route('event.join'), [
            'id' => $notikums->id
        ]);
        $response->assertJson(['Pieteicies' => 1]);
        $this->assertTrue($notikums->TgdIrPieteicies());
        $response=$this->post(route('event.join'), [
            'id' => $notikums->id
        ]);
        $this->assertFalse($notikums->TgdIrPieteicies());
        $response->assertJson(['Pieteicies' => 0]);
    }
    public function test_event_view_can_be_rendered()
    {
        $user=$this->SimulateLoggedInUser();
        $notikums=Notikums::factory()->create(['autora_id'=>$user->id]);
        $notikums->NotikumaUsers()->attach($user);
        $response=$this->get(route('event.view', $notikums->id));
        $response->assertOk();
        $response->assertViewIs('event.event');
    }
    public function test_user_can_not_view_event_if_not_invited()
    {
        $user=$this->SimulateLoggedInUser();
        $autors=User::factory()->create();
        $notikums=Notikums::factory()->create(['autora_id'=>$autors->id]);
        $response=$this->get(route('event.view', $notikums->id));
        $response->assertForbidden();
    }
    public function test_admin_can_view_other_user_event()
    {
        $user=$this->SimulateLoggedInAdmin();
        $autors=User::factory()->create();
        $notikums=Notikums::factory()->create(['autora_id'=>$autors->id]);
        $response=$this->get(route('event.view', $notikums->id));
        $response->assertOk();
        $response->assertViewIs('event.event');
    }
    public function test_event_author_can_delete_event()
    {
        $user=$this->SimulateLoggedInUser();
        $notikums=Notikums::factory()->create(['autora_id'=>$user->id]);
        $response=$this->post(route('event.delete', $notikums->id));
        $response->assertRedirect(route('home'));
        $this->assertModelMissing($notikums);
    }
    public function test_event_author_can_delete_event_with_ajax()
    {
        $user=$this->SimulateLoggedInUser();
        $notikums=Notikums::factory()->create(['autora_id'=>$user->id]);
        $response=$this->post(route('event.delete.ajax'),[
            'id'=>$notikums->id
        ], $this->ajaxHeader);
        $response->assertJson(['Id' => $notikums->id]);
        $this->assertModelMissing($notikums);
    }
    public function test_admin_can_delete_other_users_events()
    {
        $admin=$this->SimulateLoggedInAdmin();
        $autors=User::factory()->create();
        $notikums=Notikums::factory()->create(['autora_id'=>$autors->id]);
        $response=$this->post(route('event.delete.ajax'),[
            'id'=>$notikums->id
        ], $this->ajaxHeader);
        $response->assertJson(['Id' => $notikums->id]);
        $this->assertModelMissing($notikums);
    }
    public function test_user_can_not_delete_other_users_events()
    {
        $user=$this->SimulateLoggedInUser();
        $autors=User::factory()->create();
        $notikums=Notikums::factory()->create(['autora_id'=>$autors->id]);
        $response=$this->post(route('event.delete.ajax'),[
            'id'=>$notikums->id
        ], $this->ajaxHeader);
        $response->assertJsonStructure(['Error']);
        $response->assertForbidden();
        $this->assertModelExists($notikums);
    }
    public function test_user_can_leave_event()
    {
        $user=$this->SimulateLoggedInUser();
        $autors=User::factory()->create();
        $notikums=Notikums::factory()->create(['autora_id'=>$autors->id]);
        $notikums->NotikumaUsers()->attach($user);
        $response=$this->post(route('event.leave'),[
            'id'=>$notikums->id
        ], $this->ajaxHeader);
        $response->assertJson(['Id' => $notikums->id]);
        //$notikums->refresh();
        $this->assertFalse($notikums->TgdPiederNotikumam());
    }
   
    public function test_user_can_search_for_other_users_in_event()
    {
        $user = $this->SimulateLoggedInUser();
        $meklejamaisUser = User::factory()->create();
        $notikums=Notikums::factory()->create(['autora_id'=>$user->id]);
        $notikums->NotikumaUsers()->attach($user);
        $response = $this->post(route('event.search.usersAndGroups'), [
            'search' => $meklejamaisUser->name,
            'notikumsId' => $notikums->id
        ], $this->ajaxHeader);
        $response->assertJsonFragment(['id' => $meklejamaisUser->id]);
            //Pārbauda vai meklē arī daļēji uzrakstītu vārdu
        $response = $this->post(route('event.search.usersAndGroups'), [
            'search' => substr($meklejamaisUser->name,2,5),
            'notikumsId' => $notikums->id
        ], $this->ajaxHeader);
        $response->assertJsonFragment(['id' => $meklejamaisUser->id]);
    }
    public function test_user_can_search_for_groups_in_event()
    {
        $user = $this->SimulateLoggedInUser();
        $meklejamaGrupa= Grupa::factory()->create();
        $meklejamaGrupa->GrupasUsers()->attach($user);
        $notikums=Notikums::factory()->create(['autora_id'=>$user->id]);
        $notikums->NotikumaUsers()->attach($user);
        $response = $this->post(route('event.search.usersAndGroups'), [
            'search' => $meklejamaGrupa->nosaukums,
            'notikumsId' => $notikums->id
        ], $this->ajaxHeader);
        $response->assertJsonFragment(['id' => $meklejamaGrupa->id]);
            //Pārbauda vai meklē arī daļēji uzrakstītu vārdu
        $response = $this->post(route('event.search.usersAndGroups'), [
            'search' => substr($meklejamaGrupa->nosaukums,2,5),
            'notikumsId' => $notikums->id
        ], $this->ajaxHeader);
        $response->assertJsonFragment(['id' => $meklejamaGrupa->id]);
    }
    public function test_user_can_not_find_already_added_groups_and_users_in_event()
    {
        $user = $this->SimulateLoggedInUser();
        $meklejamaisUser = User::factory()->create();
        $meklejamaGrupa= Grupa::factory()->create();
        $notikums=Notikums::factory()->create(['autora_id'=>$user->id]);
        $notikums->NotikumaUsers()->attach($user);
        $notikums->NotikumaUsers()->attach($meklejamaisUser);
        $notikums->NotikumaGrupas()->attach($meklejamaGrupa);
        $responseUser = $this->post(route('event.search.usersAndGroups'), [
            'search' => $meklejamaisUser->name,
            'notikumsId' => $notikums->id
        ], $this->ajaxHeader);
        $responseGrupa = $this->post(route('event.search.usersAndGroups'), [
            'search' => $meklejamaGrupa->nosaukums,
            'notikumsId' => $notikums->id
        ], $this->ajaxHeader);
        $tuksaJsonAtbilde=[
            'Users' =>[],
            'Grupas' =>[]
        ];
        $responseUser->assertExactJson($tuksaJsonAtbilde);
        $responseGrupa->assertExactJson($tuksaJsonAtbilde);
    }
    public function test_user_can_not_search_for_blocked_users()
    {
        $user = $this->SimulateLoggedInUser();
        $meklejamaisUser = User::factory()->create();
        $tuksaJsonAtbilde=[
            'Users' =>[],
            'Grupas' =>[]
        ];
        $notikums=Notikums::factory()->create(['autora_id'=>$user->id]);
        $notikums->NotikumaUsers()->attach($user);
        $user->BloketieUsers()->attach($meklejamaisUser);
        $response = $this->post(route('event.search.usersAndGroups'), [
            'search' => $meklejamaisUser->name,
            'notikumsId' => $notikums->id
        ], $this->ajaxHeader);
        $response->assertExactJson($tuksaJsonAtbilde);
        $user->BloketieUsers()->detach($meklejamaisUser);
        $meklejamaisUser->BloketieUsers()->attach($user);
        $response = $this->post(route('event.search.usersAndGroups'), [
            'search' => $meklejamaisUser->name,
            'notikumsId' => $notikums->id
        ], $this->ajaxHeader);
        $response->assertExactJson($tuksaJsonAtbilde);
    }
    public function test_author_can_remove_group_from_event()
    {
        $user = $this->SimulateLoggedInUser();
        $grupa =Grupa::factory()->create();
        $grupasUsers = User::factory()->count(3)->create();
        $grupa->GrupasUsers()->attach($grupasUsers);
        $notikums=Notikums::factory()->create(['autora_id'=>$user->id]);
        $notikums->NotikumaUsers()->attach($user);
        $notikums->NotikumaGrupas()->attach($grupa);
        $notikums->NotikumaUsers()->attach($grupasUsers);
        $response=$this->post(route('event.remove.group'),[
            'grupaId'=>$grupa->id,
            'notikumsId'=>$notikums->id
        ]);
        $gaidamaAtbilde=[
            'nonemtie'=>[
                $grupasUsers[0]->id,
                $grupasUsers[1]->id,
                $grupasUsers[2]->id
            ],
            'grupaId' =>$grupa->id
        ];
        $response->assertExactJson($gaidamaAtbilde);
        $this->assertNotContains($grupa->id, $notikums->NotikumaGrupas()->get()->pluck('id'));
        foreach($grupasUsers as $grupasUser)
        {
            $this->assertNotContains($grupasUser->id, $notikums->NotikumaUsers()->get()->pluck('id'));
        }
    }
    public function test_author_can_remove_user_from_event()
    {
        $user = $this->SimulateLoggedInUser();
        $nonemamaisLietotajs = User::factory()->create();
        $notikums=Notikums::factory()->create(['autora_id'=>$user->id]);
        $notikums->NotikumaUsers()->attach($user);
        $notikums->NotikumaUsers()->attach($nonemamaisLietotajs);
        $response=$this->post(route('event.remove.user'),[
            'userId'=>$nonemamaisLietotajs->id,
            'notikumsId'=>$notikums->id
        ]);
        $response->assertExactJson([
            'Nonemtais'=>$nonemamaisLietotajs->id
        ]);
        $notikums->refresh();
        $this->assertNotContains($nonemamaisLietotajs->id, $notikums->NotikumaUsers()->get()->pluck('id')->toArray());
    }
    public function test_author_can_add_group_to_event()
    {
        $user = $this->SimulateLoggedInUser();
        $notikums=Notikums::factory()->create(['autora_id'=>$user->id]);
        $notikums->NotikumaUsers()->attach($user);
        $grupa=Grupa::factory()->create();
        $grupasUsers=User::factory()->count(3)->create();
        $bloketsGrupasUsers=User::factory()->create();
        $bloketsGrupasUsers->BloketieUsers()->attach($user);
        $grupa->GrupasUsers()->attach($grupasUsers);
        $grupa->GrupasUsers()->attach($bloketsGrupasUsers);
        $response=$this->post(route('event.add.group'),[
            'grupaId'=>$grupa->id,
            'notikumsId'=>$notikums->id
        ]);
        $gaidamasAtbildesStruktura=[
            'grupaId',
            'nosaukums',
            'Users' => [
                [],
                [],
                []
            ]
        ];
        $response->assertJsonStructure($gaidamasAtbildesStruktura);
        $response->assertJson([
            'grupaId' => $grupa->id,
            'nosaukums' => $grupa->nosaukums
        ]);
        $i=0;
        foreach($response->json('Users') as $user)
        {
            $this->assertEquals($grupasUsers[$i]->id, $user['id']);
            $this->assertContains($user['id'], $notikums->NotikumaUsers()->get()->pluck('id'));
            $i++;
        }
    }
    public function test_author_can_add_user_to_event()
    {
        $user = $this->SimulateLoggedInUser();
        $notikums=Notikums::factory()->create(['autora_id'=>$user->id]);
        $notikums->NotikumaUsers()->attach($user);
        $pievienojamaisUser=User::factory()->create();
        $response=$this->post(route('event.add.user'), [
            'userId'=>$pievienojamaisUser->id,
            'notikumsId'=>$notikums->id
        ]);
        $response->assertJsonFragment([
            'id' => $pievienojamaisUser->id
        ]);
        $this->assertContains($pievienojamaisUser->id, $notikums->NotikumaUsers()->get()->pluck('id'));
    }
}
