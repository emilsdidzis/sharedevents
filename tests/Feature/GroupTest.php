<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Grupa;
use Database\Factories\UserFactory;
use PHPUnit\TextUI\XmlConfiguration\Group;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GroupTest extends TestCase
{
    public function test_group_screen_can_be_rendered()
    {
        $user = $this->SimulateLoggedInUser();
        $grupa = Grupa::factory()->hasGrupasUsers($user)->create([
            'autora_id' => $user->id
        ]);


        $response = $this->get(route('group.view', $grupa->id));

        $response->assertStatus(200);
    }
    public function test_all_group_screen_can_be_rendered()
    {
        $user = $this->SimulateLoggedInUser();
        $grupas = Grupa::factory(5)->hasGrupasUsers(5)->create([
            'autora_id' => $user->id
        ]);
        foreach($grupas as $grupa)
        {
            $grupa->GrupasUsers()->attach(auth()->user());
        }
        $this->assertCount(5, auth()->user()->UseraGrupas()->get());
        $response = $this->get(route('group.index'));
        $response->assertStatus(200);
    }
    public function test_user_can_block_group()
    {
        $user = $this->SimulateLoggedInUser();
        $grupasAutors =User::factory()->create();
        $grupa = Grupa::factory()->hasGrupasUsers(5)->create([
            'autora_id' => $grupasAutors->id
        ]);
        $grupa->GrupasUsers()->attach(auth()->user());
        //Vispirms atver visu grupu skatu, lai varētu pārbaudīt redirectu.
        $this->get(route('group.index'));
        $response=$this->post(route('group.block'), ['id' =>$grupa->id]);
        $response->assertRedirect(route('group.index'));
        $this->assertContains($grupa->id, auth()->user()->BloketasGrupas()->get()->pluck('id'));
        $this->assertNotContains( auth()->user()->id, $grupa->GrupasUsers()->get()->pluck('id'));
    }
    public function test_user_can_leave_group()
    {
        $user = $this->SimulateLoggedInUser();
        $grupasAutors =User::factory()->create();
        $grupa = Grupa::factory()->hasGrupasUsers(5)->create([
            'autora_id' => $grupasAutors->id
        ]);
        $grupa->GrupasUsers()->attach(auth()->user());
        //Vispirms atver visu grupu skatu, lai varētu pārbaudīt redirectu.
        $this->get(route('group.index'));
        $response=$this->post(route('group.leave', $grupa->id));
        $response->assertRedirect(route('group.index'));
        $this->assertNotContains( auth()->user()->id, $grupa->GrupasUsers()->get()->pluck('id'));
    }
    public function test_group_edit_screen_can_be_rendered()
    {
        $response = $this->get('/login');

        $response->assertStatus(200);
    }
    public function test_group_can_be_created()
    {
        $user = $this->SimulateLoggedInUser();
        $grupasNosaukums='testaGrupa';
        $response = $this->post(route('group.store', -1), [
            'nosaukums' => $grupasNosaukums
        ]);
        $tikoIzveidotaGrupa=Grupa::where('nosaukums',$grupasNosaukums)->firstOrFail();
        $response->assertRedirect(route('group.view', $tikoIzveidotaGrupa->id));
        $this->assertDatabaseHas('grupa', ['nosaukums' =>$grupasNosaukums]);
    }
    public function test_group_can_be_edited_by_author()
    {
        $user = $this->SimulateLoggedInUser();
        $grupa=Grupa::factory()-> create([
            'autora_id' => $user->id,
        ]);
        $this->assertEquals($grupa->tgdIrAutors(), true);
        $grupasNosaukums='KautKadsNosaukums';
        $response = $this->post(route('group.store', $grupa->id), [
            'nosaukums' => $grupasNosaukums
        ]);
        $response->assertRedirect(route('group.view', $grupa->id));
        $this->assertDatabaseHas('grupa', ['id'=>$grupa->id,'nosaukums' =>$grupasNosaukums]);
    }
    public function test_group_can_not_be_edited_by_other_users()
    {
        $user = $this->SimulateLoggedInUser();
        $grupa=Grupa::factory()-> create([
            'autora_id' => User::factory()->create(),
        ]);
        $this->assertEquals($grupa->tgdIrAutors(), false);
        $grupasNosaukums='KautKadsNosaukums';
        $response = $this->post(route('group.store', $grupa->id), [
            'nosaukums' => $grupasNosaukums
        ]);
        $response->assertForbidden();
    }
    public function test_group_can_be_deleted_by_author()
    {
        $user = $this->SimulateLoggedInUser();
        $grupa=Grupa::factory()-> create([
            'autora_id' => $user->id,
        ]);
        // 'X-Requested-With'=>'XMLHttpRequest' - lai uzskatītu ka post nāk no ajax
        $response = $this->post(route('group.delete', $grupa->id), [], $this->ajaxHeader);
        $response->assertRedirect(route('group.index'));
        $this->assertDatabaseMissing('grupa', ['id'=>$grupa->id]);
    }
    public function test_group_can_be_deleted_by_author_using_ajax()
    {
        $user = $this->SimulateLoggedInUser();
        $grupa=Grupa::factory()-> create([
            'autora_id' => $user->id,
        ]);
        // 'X-Requested-With'=>'XMLHttpRequest' - lai uzskatītu ka post nāk no ajax
        $response = $this->post(route('group.delete.ajax'), ['id'=>$grupa->id], $this->ajaxHeader);
        $response->assertJson(['Id'=>$grupa->id]);
        $response->assertSuccessful();
        $this->assertDatabaseMissing('grupa', ['id'=>$grupa->id]);
    }
    
    public function test_group_can_not_be_deleted_by_other_users_using_ajax()
    {
        $user = $this->SimulateLoggedInUser();
        $grupa=Grupa::factory()-> create([
            'autora_id' => User::factory()->create()->id,
        ]);
        // 'X-Requested-With'=>'XMLHttpRequest' - lai uzskatītu ka post nāk no ajax
        $response = $this->post(route('group.delete.ajax'), ['id'=>$grupa->id], $this->ajaxHeader);
        $response->assertJsonMissing(['id'=>$grupa->id]);
        $response->assertForbidden();
        $this->assertDatabaseHas('grupa', ['id'=>$grupa->id]);
    }
    public function test_group_can_not_be_deleted_if_request_is_not_from_ajax()
    {
        $user = $this->SimulateLoggedInUser();
        $grupa=Grupa::factory()-> create([
            'autora_id' => $user->id,
        ]);
        // 'X-Requested-With'=>'XMLHttpRequest' - lai uzskatītu ka post nāk no ajax
        $response = $this->post(route('group.delete.ajax'), ['id'=>$grupa->id]);
        $response->assertForbidden();
        $this->assertDatabaseHas('grupa', ['id'=>$grupa->id]);
    }
    public function test_users_can_be_added_to_group()
    {
        $user = $this->SimulateLoggedInUser();
        $grupasBiedrs = User::findOrFail(User::factory()->create()->id);
        $grupa=Grupa::factory()-> create([
            'autora_id' => $user->id,
        ]);
        $grupa->GrupasUsers()->attach($user);
        $response = $this->post(route('group.add.user'), [
            'grupaId'=>$grupa->id,
            'userId' =>$grupasBiedrs->id
        ]);
        $response->assertJsonFragment(['id' => $grupasBiedrs->id]);
        $this->assertDatabaseHas('piederiba_grupai', ['users_id'=>$grupasBiedrs->id, 'grupa_id'=>$grupa->id]);
    }
    public function test_user_can_not_be_added_to_a_blocked_group()
    {
        $user = $this->SimulateLoggedInUser();
        $grupasBiedrs = User::findOrFail(User::factory()->create()->id);
        $grupa=Grupa::factory()-> create([
            'autora_id' => $user->id,
        ]);
        $grupa->GrupasUsers()->attach($user);
        $grupasBiedrs->BloketasGrupas()->attach($grupa->id);
        $response = $this->post(route('group.add.user'), [
            'grupaId'=>$grupa->id,
            'userId' =>$grupasBiedrs->id
        ]);
        $response->assertJson(['user' => null]);
        $this->assertDatabaseMissing('piederiba_grupai', ['users_id'=>$grupasBiedrs->id, 'grupa_id'=>$grupa->id]);
    }
    public function test_user_can_not_be_added_to_a_group_by_blocked_user()
    {
        $user = $this->SimulateLoggedInUser();
        $grupasBiedrs = User::findOrFail(User::factory()->create()->id);
        $grupa=Grupa::factory()-> create([
            'autora_id' => $user->id,
        ]);
        $grupa->GrupasUsers()->attach($user);
        $grupasBiedrs->BloketieUsers()->attach($user->id);
        $response = $this->post(route('group.add.user'), [
            'grupaId'=>$grupa->id,
            'userId' =>$grupasBiedrs->id
        ]);
        $response->assertJson(['user' => null]);
        $this->assertDatabaseMissing('piederiba_grupai', ['users_id'=>$grupasBiedrs->id, 'grupa_id'=>$grupa->id]);
    }
    public function test_group_user_search_can_find_user_by_name()
    {
        $user = $this->SimulateLoggedInUser();
        $meklejamaisUser = User::factory()->create();
        $grupa=Grupa::factory()-> create([
            'autora_id' => $user->id,
        ]);
        $response = $this->post(route('group.search.users'), [
            'search' => $meklejamaisUser->name,
            'grupaId' => $grupa->id
    ], $this->ajaxHeader);
        $response->assertJson(['Users' => [$meklejamaisUser->jsonSerialize()]]);
    }
    public function test_user_is_not_searchable_by_blocked_user_in_group()
    {
        $user = $this->SimulateLoggedInUser();
        $meklejamaisUser = User::factory()->create();
        $grupa=Grupa::factory()-> create([
            'autora_id' => $user->id,
        ]);
        $meklejamaisUser->BloketieUsers()->attach($user);
        $response = $this->post(route('group.search.users'), [
            'search' => $meklejamaisUser->name,
            'grupaId' => $grupa->id
    ], $this->ajaxHeader);
        $response->assertJson(['Users' => null]);
    }
    public function test_user_is_not_searchable_by_blocked_group()
    {
        $user = $this->SimulateLoggedInUser();
        $meklejamaisUser = User::factory()->create();
        $grupa=Grupa::factory()-> create([
            'autora_id' => $user->id,
        ]);
        $meklejamaisUser->BloketasGrupas()->attach($grupa);
        $response = $this->post(route('group.search.users'), [
            'search' => $meklejamaisUser->name,
            'grupaId' => $grupa->id
    ], $this->ajaxHeader);
        $response->assertJson(['Users' => null]);
    }
}
