<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Str;
use Illuminate\Http\UploadedFile;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UploadTest extends TestCase
{
    public function test_ckeditor_files_can_be_successfully_uploaded()
    {
        $user=$this->SimulateLoggedInUser();
        $nosaukumsBildei=Str::random(10).'.jpg';
        $image=UploadedFile::fake()->image($nosaukumsBildei, 100, 100);
        $response=$this->post(route('CKEUpload'),[
            'upload'=>$image
        ]);
        $response->assertJson(['uploaded'=>'true']);
    }
}
