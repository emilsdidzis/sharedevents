<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Notikums;
use App\Models\Komentars;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CommentTest extends TestCase
{
    public function test_user_can_add_comment_on_event()
    {
        $user=$this->SimulateLoggedInUser();
        $autors=User::factory()->create();
        $notikums=Notikums::factory()->create(['autora_id'=>$autors->id]);
        $notikums->NotikumaUsers()->attach($user);
        $komentars=fake()->realTextBetween(3, 500);
        $response=$this->post(route('comment.add', $notikums->id), [
            'saturs'=>$komentars
        ]);
        $response->assertRedirect(route('event.view', [$notikums->id]));
        $this->assertDatabaseHas('komentars',[
            'autora_id' =>$user->id,
            'notikums_id' =>$notikums->id,
            'saturs' => $komentars
        ]);
    }
    public function test_user_can_delete_comment_from_event()
    {
        $user=$this->SimulateLoggedInUser();
        $autors=User::factory()->create();
        $notikums=Notikums::factory()->create(['autora_id'=>$autors->id]);
        $notikums->NotikumaUsers()->attach($user);
        $komentars=Komentars::factory()->create([
            'autora_id'=>$user->id,
            'notikums_id'=>$notikums->id
        ]);
        $response=$this->post(route('comment.delete', $komentars->id), [], $this->ajaxHeader);
        $this->assertModelMissing($komentars);
    }
    public function test_user_can_not_delete_other_users_comments()
    {
        $user=$this->SimulateLoggedInUser();
        $autors=User::factory()->create();
        $notikums=Notikums::factory()->create(['autora_id'=>$autors->id]);
        $notikums->NotikumaUsers()->attach($user);
        $komentars=Komentars::factory()->create([
            'autora_id'=>$autors->id,
            'notikums_id'=>$notikums->id
        ]);
        $response=$this->post(route('comment.delete', $komentars->id), [], $this->ajaxHeader);
        $response->assertForbidden();
        $this->assertModelExists($komentars);
    }
}
