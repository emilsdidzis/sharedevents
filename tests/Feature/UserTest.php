<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    public function test_user_can_view_other_users()
    {
        $user = User::factory()->create();
        $this->view_can_be_rendered_by_user(route('user.view', $user->id));
    }
    public function test_user_index_view_can_be_rendered()
    {
        $this->view_can_be_rendered_by_user(route('user.index'));
    }
    public function test_user_can_search_for_other_users()
    {
        $user = $this->SimulateLoggedInUser();
        $meklejamaisUser = User::factory()->create();
        $response = $this->post(route('user.search'), [
            'search' => $meklejamaisUser->name
    ], $this->ajaxHeader);
    $response->assertJsonFragment(['id' => $meklejamaisUser->id]);
        //Pārbauda vai meklē arī daļēji uzrakstītu vārdu
        $response = $this->post(route('user.search'), [
            'search' => substr($meklejamaisUser->name,2,5)
    ], $this->ajaxHeader);
    $response->assertJsonFragment(['id' => $meklejamaisUser->id]);
    }
    public function test_user_can_not_search_for_blocked_user()
    {
        $user = $this->SimulateLoggedInUser();
        $meklejamaisUser = User::factory()->create();
        $meklejamaisUser->BloketieUsers()->attach($user);
        $response = $this->post(route('user.search'), [
            'search' => $meklejamaisUser->name
    ], $this->ajaxHeader);
        $response->assertExactJson(['Users' => []]);
    }
    public function test_user_search_minimum_character_count_is_3()
    {
        $user = $this->SimulateLoggedInUser();
        $meklejamaisUser = User::factory()->create();
        $response = $this->post(route('user.search'), [
            'search' => substr($meklejamaisUser->name, 0, 2)
    ], $this->ajaxHeader);
        $response->assertExactJson(['Users' => []]);
        $response = $this->post(route('user.search'), [
            'search' => substr($meklejamaisUser->name, 0, 3)
    ], $this->ajaxHeader);
    $response->assertJsonFragment(['id' => $meklejamaisUser->id]);
    }
    public function test_user_can_block_and_unblock_another_user()
    {
        $user=$this->SimulateLoggedInUser();
        $blokejamaisUser=User::factory()->create();
        $response=$this->post(route('user.block'), [
            'blokejamais' =>$blokejamaisUser->id,
            'bloket' => 1
        ]);
        $this->assertTrue($user->BloketieUsers()->get()->contains($blokejamaisUser));
        $response=$this->post(route('user.block'), [
            'blokejamais' =>$blokejamaisUser->id,
            'bloket' => 0
        ]);
        $this->assertFalse($user->BloketieUsers()->get()->contains($blokejamaisUser));

    }
    public function test_user_edit_view_can_be_rendered()
    {
        $user=$this->SimulateLoggedInUser();
        $response=$this->get(route('user.edit', $user->id));
        $response->assertViewIs('user.user_edit');
        $response->assertOk();
    }
    public function test_user_can_be_edited()
    {
        $user=$this->SimulateLoggedInUser();
        $editojamasVertibas=array(
            'name' => 'Vards OtrsVards Uzvards',
            'sanemt_pazinojumus' => 1,
            'pazinot_pirms' =>rand(1,10000),
            'avatar_url' => 'https://example.com/avatar.png',
        );
        $response=$this->post(route('user.store', $user->id), $editojamasVertibas);
        $response->assertRedirect(route('user.view', $user->id));
        //Atjauno datus pēc rediģēšanas no DB
        $user->refresh();
        $this->assertTrue($user->name==$editojamasVertibas['name']&&
                          $user->sanemt_pazinojumus==$editojamasVertibas['sanemt_pazinojumus'] &&
                          $user->pazinot_pirms==$editojamasVertibas['pazinot_pirms'] &&
                          $user->avatar_url==$editojamasVertibas['avatar_url'] );
    }
    public function test_user_can_be_edited_by_admin()
    {
        $this->SimulateLoggedInAdmin();
        $editojamaisUser=User::factory()->create();
        $editojamasVertibas=array(
            'name' => 'Vards OtrsVards Uzvards',
            'sanemt_pazinojumus' => 1,
            'pazinot_pirms' =>rand(1,10000),
            'avatar_url' => 'https://example.com/avatar.png',
            'email' => 'piemers@example.lv'
        );
        $response=$this->post(route('user.store', $editojamaisUser->id), $editojamasVertibas);
        $response->assertRedirect(route('user.view', $editojamaisUser->id));
        //Atjauno datus pēc rediģēšanas no DB
        $editojamaisUser->refresh();
        $this->assertTrue($editojamaisUser->name==$editojamasVertibas['name']&&
                          $editojamaisUser->sanemt_pazinojumus==$editojamasVertibas['sanemt_pazinojumus'] &&
                          $editojamaisUser->pazinot_pirms==$editojamasVertibas['pazinot_pirms'] &&
                          $editojamaisUser->avatar_url==$editojamasVertibas['avatar_url'] &&
                          $editojamaisUser->email==$editojamasVertibas['email']);
    }
    public function test_admin_can_make_other_users_admins()
    {
        $this->SimulateLoggedInAdmin();
        $toposaisAdmin = User::factory()->create();
        $response=$this->post(route('user.store', $toposaisAdmin->id), [
            'name' => $toposaisAdmin->name,
            'pazinot_pirms' => $toposaisAdmin->pazinot_pirms,
            'avatar_url' => $toposaisAdmin->avatar_url,
            'email' => $toposaisAdmin->email,
            'is_admin' =>1
        ]);
        $response->assertRedirect(route('user.view', $toposaisAdmin->id));
        $toposaisAdmin->refresh();
        $this->assertTrue($toposaisAdmin->is_admin==1);
    }
}
