<?php

namespace App\Console\Commands;

use App\Http\Controllers\Webex\WebexAuthController;
use Illuminate\Console\Command;

class RefreshWebexTokens extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'webextokens:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        WebexAuthController::checkTokensExpireDate();
        return Command::SUCCESS;
    }
}
