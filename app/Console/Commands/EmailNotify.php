<?php

namespace App\Console\Commands;

use DateTime;
use App\Models\User;
use App\Models\Notikums;
use Mockery\Matcher\Not;
use App\Mail\NotifyEmail;
use Illuminate\Support\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class EmailNotify extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:notify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends emails to users about upcoming events';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $count = 0;
        $users=User::where('sanemt_pazinojumus', '1')->get();
        foreach ($users as $user)
        {
            $tagad=Carbon::now();
            $pazinotPirms=Carbon::now()->addMinutes($user->pazinot_pirms);
            $notikumi=$user->UseraNotikumi()->wherePivot('pazinots', 0)-> whereBetween('sakums', [$tagad, $pazinotPirms])->get();
            foreach($notikumi as $notikums)
            {
                Mail::to($user->email)->queue(new NotifyEmail($notikums, $user));
                $count++;
                $notikums->pivot->pazinots=1;
                $notikums->pivot->save();
            }

        }
        $this->info('Sent ' . $count . ' emails');
        return 0;
    }
}
