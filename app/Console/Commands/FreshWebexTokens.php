<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Webex\WebexAuthController;

class FreshWebexTokens extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'webextokens:fresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        WebexAuthController::refreshAllTokens();
        return Command::SUCCESS;
    }
}
