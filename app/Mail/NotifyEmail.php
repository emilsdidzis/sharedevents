<?php

namespace App\Mail;

use Tio\Laravel\Facade as Translation;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($notikums, $user)
    {
        $this->notikums=$notikums;
        $this->user=$user;
    }
    public $notikums;
    public $user;
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        Translation::setLocale($this->user->valoda);
        return $this->from(env('MAIL_FROM_ADDRESS'))->view('email')->subject(t('Notification about event starting soon!'));
    }
}
