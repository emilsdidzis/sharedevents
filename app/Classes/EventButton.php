<?php
namespace App\Classes;

class EventButton extends Button{
    public $noformejumi=[
        'card-btn' =>'px-4 py-2 bg-gray-500 shadow-lg border rounded-lg text-white uppercase font-semibold tracking-wider focus:shadow-outline hover:bg-teal-400 active:bg-teal-400',
        'event-view-btn' => 'px-4 py-2 bg-gray-500 shadow-lg border rounded-lg text-white uppercase font-semibold tracking-wider focus:shadow-outline hover:bg-teal-400 active:bg-teal-400'
    ];
    public function __construct($title, $href='',$id=-1,$tips='',$class='', $noformejums='card-btn')
    {
        parent::__construct($title, $href,$id,$tips,$class);
        $this->class=$this->class.' '.$this->noformejumi[$noformejums];
    }
}