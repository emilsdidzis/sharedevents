<?php
namespace App\Classes;

class Button{
    public $title;
    public $href;
    public $id;
    public $tips;
    public $class;
    
    public function __construct($title, $href='',$id=-1,$tips='',$class=''){
        $this->title=$title;
        $this->href=$href;
        $this->id=$id;
        $this->tips=$tips;
        $this->class=$class;
    }
}
?>