<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LanguageController extends Controller
{
    private $validLanguages = array('lv', 'en');
    public function setLanguage($valoda)
    {
        if (!in_array($valoda, $this->validLanguages))
            abort(403);
        session(['locale' => $valoda]);
        $user=auth()->user();
        if ($user!=NULL)
        {
            $user->valoda=$valoda;
            $user->save();
        }
        return redirect()->back();
    }
}
