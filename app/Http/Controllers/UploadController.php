<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UploadController extends Controller
{
    private function CheckExtensionForImage($ext)
    {
        $supported_image = array(
            'gif',
            'jpg',
            'jpeg',
            'png',
            'apng',
            'jfif',
            'pjpeg',
            'pjp'
        );
        $ext=strtolower($ext);
        if (in_array($ext, $supported_image)) {
            return true;
        } else {
            return false;
        }
    }
    public function CKEUpload(Request $request)
    {
        if($request->hasFile('upload')) {
            //get filename with extension
            $filenamewithextension = $request->file('upload')->getClientOriginalName();
      
            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
      
            //get file extension
            $extension = $request->file('upload')->getClientOriginalExtension();
      
            //filename to store
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $filenametostore = $filename.'_'.time().'.'.$extension;
            if ($this->CheckExtensionForImage($extension) && $request->file('upload')->getSize()<=3145728) {
                //Upload File
                $request->file('upload')->storeAs('public/uploads', $filenametostore);
 
                $url = asset('storage/uploads/'.$filenametostore);
                $msg = t('Image successfully uploaded');
                //$msg = strval($request->file('upload')->getSize());
                $re=[
                    'uploaded' => 'true',
                    'url' => $url
                ];
            }
            else
            {
                $url='';
                if ($this->CheckExtensionForImage($extension))
                {
                    $msg=t('Image should be smaller than 3Mb');
                }
                else
                {
                    $msg=t('Uploaded image format is not supported! Supported formats: ');
                    $supported_image = array(
                        'gif',
                        'jpg',
                        'jpeg',
                        'png',
                        'apng',
                        'jfif',
                        'pjpeg',
                        'pjp'
                    );
                    $first=true;
                    foreach ($supported_image as $format)
                    {
                        if (!$first)
                        {
                            $msg=$msg.', ';
                        }
                        $first=false;
                        $msg=$msg.$format;
                    }
                    
                }
                $re=[
                    //'uploaded' => 'false',
                    'error' => [
                        'message' => $msg
                    ]
                ];
            }
            return response()->json($re);
        }
    }
    function ajaxUpload(Request $request)
    {
     $validation = Validator::make($request->all(), [
      'select_file' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
     ]);
     if($validation->passes())
     {
      $image = $request->file('select_file');
      $new_name = rand() . '.' . $image->getClientOriginalExtension();
      $image->move(public_path('images/uploaded'), $new_name);
      return response()->json([
       'message'   => t('Image Upload Successfully'),
       'uploaded_image' => '/images/uploaded/'.$new_name,
       'class_name'  => 'alert-success'
      ]);
     }
     else
     {
      return response()->json([
       'message'   => $validation->errors()->all(),
       'uploaded_image' => '',
       'class_name'  => 'alert-danger'
      ]);
     }
    }
}
