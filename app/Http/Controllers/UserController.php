<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct() {
        // only Admins have access to the following methods
        //$this->middleware('auth.admin')->only(['create', 'store']);
        // only authenticated users have access to the methods of the controller
        $this->middleware(['auth', 'verified']);
    }
    public function userDashboard()
    {
        return view('user.user_dashboard');
    }
    public function userSelfProfile()
    {
        return $this->view(-1);
    }
    public function view($id=-1)
    {
        if ($id==-1)
            $id=auth()->user()->id;
        $user=User::findOrFail($id);
        return view('user.user', compact(['user']));
    }
    public function index()
    {
        return view('user.users');
    }
    public function search(Request $request)
    {
        if ($request->ajax()) {
            if (strlen($request->search) < 3 || strlen($request->search) > 255) {
                return response()->json(['Users' => []], 200);
            }
            $bloketaji = auth()->user()->NoblokejusieUsers()->get()->pluck('id')->toArray();
            $UserMeklRez = User::where('name', 'LIKE', '%' . $request->search . "%")->whereNotIn('id', $bloketaji)->get();
            return response()->json(['Users' => $UserMeklRez], 200);
        }
    }
    public function block(Request $request)
    {
        $user=auth()->user();
        $blokejamais=User::findOrFail($request->blokejamais);
        if ($request->bloket=='1')
        {
            if (!$user->BloketieUsers()->get()->contains($blokejamais))
                $user->BloketieUsers()->attach($blokejamais);
        }
        else
        {
            if ($user->BloketieUsers()->get()->contains($blokejamais))
                $user->BloketieUsers()->detach($blokejamais);
        }
        return redirect()->back();
    }
    public function edit($id)
    {
        if (!auth()->user()->isAdmin() && $id!=auth()->user()->id)
            abort(403);

        $user=User::findOrFail($id);
        return view('user.user_edit', compact(['user']));
    }
    public function store(Request $request, $id)
    {
        if ($id!=auth()->user()->id && !auth()->user()->isAdmin())
            abort(403);
        $user=User::findOrFail($id);
        $rules = array(
            'name' => 'required|string|min:1|max:255',
            'pazinot_pirms' => 'required|integer|between:1,10000',
            'avatar_url' => 'string|max:201',
        );
        if (auth()->user()->isAdmin())
            $rules['email'] = 'email:rfc,dns|required|min:3';
        $this->validate($request, $rules);
        $request->sanemt_pazinojumus = $request->sanemt_pazinojumus == null ? 0 : 1;
        $user->name=$request->name;
        $user->sanemt_pazinojumus=$request->sanemt_pazinojumus;
        $user->pazinot_pirms=$request->pazinot_pirms;
        if ($request->avatar_url!=null)
            $user->avatar_url=$request->avatar_url;
        if (auth()->user()->isAdmin())
        {
            $request->is_admin = $request->is_admin == null ? 0 : 1;
            $user->email=$request->email;
            $user->is_admin=$request->is_admin;
        }
        $user->save();
        return redirect()->route('user.view', [$user->id]);
    }
    public function delete(Request $request)
    {
        $user=User::findOrFail($request->id);
        if ($user->id!=auth()->user()->id && !auth()->user()->isAdmin())
            abort(403);
        if ($user->id==auth()->user()->id)
            auth()->logout();
        $user->delete();
        return redirect()->route('home');
    }
}
