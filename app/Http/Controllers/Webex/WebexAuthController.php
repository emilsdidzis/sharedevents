<?php

namespace App\Http\Controllers\Webex;

use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\WebexUserTokens;
use App\Http\Controllers\Controller;
use Http;
use Illuminate\Support\Facades\Redirect;
use Laravel\Socialite\Facades\Socialite;

class WebexAuthController extends Controller
{
    private static $refreshTokenUrl="https://webexapis.com/v1/access_token";
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    
    public function redirectToWebexAuth()
    {
        session()->put('intended_url', url()->previous());
        return Socialite::driver('webex')->setScopes(['meeting:schedules_read',
        'meeting:schedules_write',
        'meeting:preferences_read',
        'spark:people_read'])
        ->redirect();
    }
    public function handleCallback()
    {
        try {
            $webexUser = Socialite::driver('webex')->user();
        } catch (Exception $e) {
            if (env('APP_ENV')=='local')
            {
                dump($e);
                return;
            }
            return redirect(route('error',['error' => t('Something went wrong!')]));
        }
        $user=auth()->user();
        if ($user==null)
            abort(403);
        if (sizeof($user->WebexTokens()->get())>0)
            $user->WebexTokens()->delete();
        $webexUserTokens=new WebexUserTokens();
        $webexUserTokens->user_id=$user->id;
        $webexUserTokens->access_token=$webexUser->token;
        $webexUserTokens->refresh_token=$webexUser->refreshToken;
        $webexUserTokens->expires=Carbon::now()->addMinutes($webexUser->expiresIn-1);
        $webexUserTokens->refresh_token_expires=Carbon::now()->addMinutes($webexUser->accessTokenResponseBody['refresh_token_expires_in']-1);
        $webexUserTokens->save();
        $intendedUrl = session('intended_url');
        return Redirect::intended($intendedUrl);
    }
    public static function checkTokensExpireDate()
    {
        $termins=Carbon::now()->addDays(7);
        $webexUserTokens=WebexUserTokens::where('expires', '<', $termins)->orWhere('refresh_token_expires', '<', $termins)->get();
        foreach ($webexUserTokens as $tokens)
        {
            self::refreshTokens($tokens);
        }
    }
    public static function refreshAllTokens()
    {
        $webexUserTokens=WebexUserTokens::get();
        foreach ($webexUserTokens as $tokens)
        {
            self::refreshTokens($tokens);
        }
    }
    private static function refreshTokens(WebexUserTokens $webexUserTokens)
    {
        if ($webexUserTokens->refresh_token_expires<Carbon::now())
        {
            $webexUserTokens->delete();
            return;
        }
        try
        {
            $response=Http::post(self::$refreshTokenUrl,
                [
                    'grant_type' => 'refresh_token',
                    'client_id' => env('WEBEX_CLIENT_ID'),
                    'client_secret' => env('WEBEX_CLIENT_SECRET'),
                    'refresh_token' => $webexUserTokens->refresh_token
                ]
            );
        }
        catch (Exception $e)
        {
            if (env('APP_ENV')=='local')
            {
                dump($e);
                return;
            }
        }
        if ($response->status()==200)
        {
            $responseData=$response->json();
            $webexUserTokens->access_token=$responseData['access_token'];
            $webexUserTokens->refresh_token=$responseData['refresh_token'];
            $webexUserTokens->expires=Carbon::now()->addMinutes($responseData['expires_in']-1);
            $webexUserTokens->refresh_token_expires=Carbon::now()->addMinutes($responseData['refresh_token_expires_in']-1);
            $webexUserTokens->save();
        }
    }
}
