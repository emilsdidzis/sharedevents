<?php

namespace App\Http\Controllers\Webex;

use Exception;
use App\Models\Notikums;
use App\Models\WebexMeeting;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\WebexUserTokens;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Config;

class WebexMeetingController extends Controller
{
    public static $webexMeetingUrl='https://webexapis.com/v1/meetings';
    public static $webexGetSitesUrl='https://webexapis.com/v1/meetingPreferences/sites';
    public static $webexDeleteMeetingUrl='https://webexapis.com/v1/meetings/';
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    //Funkcija nosaka vai ir jāpievieno, jānoņem Webex sapulce vai arī vispār
    //nekas nav jādara
    public static function handleMeetingForEvent(Notikums $notikums, bool $pievienot)
    {
        if ($notikums==null)
        {
            return t('Event does not exist!');
        }
        $webexMeeting=$notikums->WebexMeeting()->get();
        if (sizeof($webexMeeting)>0 && !$pievienot)
        {
            self::deleteWebexMeetingFromEvent($notikums);
            return 0;
        }
        if (sizeof($webexMeeting)==0 && $pievienot)
        {
            if (sizeof(auth()->user()->WebexTokens()->get())==0)
                return t('You need to connect your Webex account first!');
            if ($notikums->beigas <Carbon::now())
                return t('Can not create Webex meeting for past event!');
            return WebexMeetingController::createMeeting($notikums);
        }
        return 0;
    }
    //Webex sapulces tiek glabātas apakšdomēnos, piemēram,
    //meeting100.webex.com un ir jāiegūst lietotāja saite
    //kurā būtu jāizveido sapulce, jo bez šīs saites
    //sapulci nevar izveidot
    private static function getMeetingSiteUrl(WebexUserTokens $webexUserTokens)
    {
        try{
        $response= Http::withHeaders(
            [
                'Authorization' => 'Bearer '.$webexUserTokens->access_token
            ]
            )->get(self::$webexGetSitesUrl);
        }
        catch(Exception)
        {
            return t('Could not create Webex meeting, something went wrong. Maybe try to connect your Webex account again!');
        }
        $atbildesJson=$response->json();
        if (sizeof($atbildesJson['sites'])==0)
            return null;
        else
        {
            return $atbildesJson['sites'][0]['siteUrl'];
        }
    }
    //Galvenā Webex sapulces izveides funkcija
    private static function createMeeting(Notikums $notikums)
    {
        if (sizeof(auth()->user()->WebexTokens()->get())==0)
            return t('Please connect your Webex account first!');
       $webexTokens=auth()->user()->WebexTokens()->get()[0];
        
        $meetingSiteUrl=self::getMeetingSiteUrl($webexTokens);
        if ($meetingSiteUrl==null)
            return t('Could not create Webex meeting, something went wrong. Maybe try to connect your Webex account again!');
        $meetingSakums=max(Carbon::create($notikums->sakums), Carbon::now()->addMinute());
        $meetingBeigas=min(max(Carbon::create($notikums->beigas), Carbon::now()->addMinutes(11)), $meetingSakums->copy()->addHours(23)->addMinutes(59));
       try
       {
        //Tiek mēģināts sūtīt sapulces izveides API pieprasījums uz Webex
            $response= Http::withHeaders(
                [
                    'Authorization' => 'Bearer '.$webexTokens->access_token,
                    'Content-Type' => 'application/json'
                ]
            )->post(WebexMeetingController::$webexMeetingUrl, [
                'title' => $notikums->virsraksts,
                'start' => $meetingSakums->toIso8601String(),
                'end' => $meetingBeigas->toIso8601String(),
                'timezone' => Config::get('app.timezone'),
                'siteUrl' => $meetingSiteUrl,
                //'unlockedMeetingJoinSecurity' =>
            ]);
       }
       catch (Exception $e)
        {
            return t('Could not create Webex meeting, something went wrong. Maybe try to connect your Webex account again!');
        }
        $atbildesJson=$response->json();
        if (array_key_exists('errors', $atbildesJson) && sizeof($atbildesJson['errors'])>0)
            return t('Could not create Webex meeting, something went wrong. Maybe try to connect your Webex account again!');
        $webexMeeting = new WebexMeeting();
        $webexMeeting->join_url=$atbildesJson['webLink'];
        $webexMeeting->meeting_id=$atbildesJson['id'];
        $webexMeeting->save();
        $notikums->webex_meeting_id=$webexMeeting->id;
        return 0;
    }
    //Webex sapulces dzēšanas funkcija, kas izdzēš gan no datu bāzes
    //gan no lietotāja Webex konta
    public static function deleteWebexMeetingFromEvent(Notikums $notikums)
    {
        if (sizeof($notikums->WebexMeeting()->get())==0)
            return 0;
        $webexMeetingId=$notikums->WebexMeeting()->first()->meeting_id;
        $webexMeeting=$notikums->WebexMeeting()->first();
        $notikums->webex_meeting_id=null;
        $notikums->save();
        $webexMeeting->delete();
        if (sizeof($notikums->Autors()->get())==0)
            return 0;
        $autors=$notikums->Autors()->first();
        if (sizeof($autors->WebexTokens()->get())==0)
            return 0;
        $webexTokens=$autors->WebexTokens()->first();
        $response= Http::withHeaders(
            [
                'Authorization' => 'Bearer '.$webexTokens->access_token,
                'Content-Type' => 'application/json'
            ]
        )->delete(self::$webexDeleteMeetingUrl.$webexMeetingId);
        return 0;
    }
}
