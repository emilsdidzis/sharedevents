<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Grupa;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    public function view($id)
    {
        $grupa=Grupa::findOrFail($id);
        return view('group.group', compact(['grupa']));
    }
    public function index()
    {
        $grupas=auth()->user()->UseraGrupas()->get();
        return view('group.groups', compact(['grupas']));
    }
    public function leave(Request $request, $bloket=false)
    {
        $user=auth()->user();
        $grupa=Grupa::find($request->id);
        if ($user==null || $grupa==null || $grupa->TgdIrAutors() || !$grupa->GrupasUsers()->get()->contains($user))
            return redirect()->back();
        if ($bloket)
            $user->BloketasGrupas()->attach($grupa);
        $grupa->GrupasUsers()->detach($user);
        $this->AtvienotUserNoGrupasNakotnesNotikumiem($user,$grupa);

        return redirect()->back();
    }
    public function block(Request $request)
    {
        return $this->leave($request, true);
    }
    public function edit($id=-1)
    {
        $grupa = Grupa::find($id);
        if ($id != -1) {
            $this->TgdIrAutorsVaiAdminsParbaude($grupa);
        }
        if ($id == -1) {
            $grupa = new Grupa();
            foreach ($grupa->all() as $kolonna) {
                $kolonna = NULL;
            }
        }
        return view('group.group_edit', compact('grupa', 'id'));
    }
    public function store(Request $request, $id)
    {
        $rules = array(
            'nosaukums' => 'required|string|min:3|max:50',
        );
        $this->validate($request, $rules);

        if ($id == -1) {
            $grupa = new Grupa();
            $grupa->autora_id = auth()->user()->id;
            $jauns = true;
        } else {
            $grupa = Grupa::findOrFail($id);
            $this->TgdIrAutorsVaiAdminsParbaude($grupa);
            $jauns = false;
        }
        $grupa->nosaukums = $request->nosaukums;
        $grupa->save();
        if ($jauns)
            $grupa->GrupasUsers()->attach(auth()->user());
        return redirect()->route('group.view', [$grupa->id]);
    }
    public function deleteAjax(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->id;
            $grupa = Grupa::findOrFail($id);
            if ($grupa->TgdIrAutors()) {
                $grupa->delete();
                return response()->json(['Id' => $id], 200);
            }
            return response()->json(['Error' => t('Logged in user is not author of the group!')], 403);
        }
        abort(403);
    }
    public function delete(Request $request)
    {
        $id = $request->id;
        $grupa = Grupa::findOrFail($id);
        $this->TgdIrAutorsVaiAdminsParbaude($grupa);
        $grupa->delete();
        return redirect()->route('group.index');

    }
    public function removeUser(Request $request)
    {
        $user = User::findOrFail($request->userId);
        $grupa = Grupa::findOrFail($request->grupaId);
        $this->TgdIrAutorsVaiAdminsParbaude($grupa);
        if (!$grupa->GrupasUsers()->get()->contains($user))
            return response()->json(['Error' => t('User was not found in the group')], 404);
        if ($grupa->autora_id != $user->id)
            {
                $grupa->GrupasUsers()->wherePivot('users_id', '=', $user->id)->detach();
                $this->AtvienotUserNoGrupasNakotnesNotikumiem($user, $grupa);
            }
        else
            return response()->json(['Error' => t('Can not remove user because user is author of the group!')], 403);
        return response()->json([
            'Nonemtais' => $user->id,
        ], 200);
    }
    public function addUser(Request $request)
    {
        $user = User::findOrFail($request->userId);
        $grupa = Grupa::findOrFail($request->grupaId);
        $this->TgdIrAutorsVaiAdminsParbaude($grupa);
        if (
            !$user->BloketasGrupas()->get()->contains($grupa) &&
            !auth()->user()->NoblokejusieUsers()->get()->contains($user) &&
            !$grupa->GrupasUsers()->get()->contains($user)
        ) {
            $grupa->GrupasUsers()->attach($user);
            $this->PievienotUserGrupasNakotnesNotikumiem($user, $grupa);
            return response()->json([
                'user' => [$user],
            ], 200);
        }
        return response()->json([
            'user' => null
        ], 403);
    }
    public function searchUsers(Request $request)
    {
        if ($request->ajax()) {
            if (strlen($request->search) < 3 || strlen($request->search) > 255) {
                return response()->json(['Users' => []], 200);
            }
            $grupa = Grupa::findOrFail($request->grupaId);
            $this->tgdIrAutorsVaiAdminsParbaude($grupa);
            $grupaUsers = $grupa->GrupasUsers()->get()->pluck('id')->toArray();
            $bloketaji = auth()->user()->NoblokejusieUsers()->get()->pluck('id')->toArray();
            $grupasBloketaji = $grupa->BloketieUsers()->get()->pluck('id')->toArray();
            $UserMeklRez = User::where('name', 'LIKE', '%' . $request->search . "%")->whereNotIn('id', $grupaUsers)->whereNotIn('id', $bloketaji)->whereNotIn('id', $grupasBloketaji)->get();
            return response()->json(['Users' => $UserMeklRez->jsonSerialize()], 200);
        }
    }
    private function AtvienotUserNoGrupasNakotnesNotikumiem(User $user, Grupa $grupa)
    {
        $grupasNakotnesNotikumi=$grupa->GrupasNotikumi()->get()->where('sakums'>=now());
        $user->UseraNotikumi()->detach($grupasNakotnesNotikumi);
    }
    private function PievienotUserGrupasNakotnesNotikumiem(User $user, Grupa $grupa)
    {
        $grupasNakotnesNotikumi=$grupa->GrupasNotikumi()->get()->where('sakums'>=now());
        $user->UseraNotikumi()->attach($grupasNakotnesNotikumi);
    }
    private function TgdIrAutorsVaiAdminsParbaude(Grupa $grupa)
    {
        if (!$grupa->TgdIrAutors() && !auth()->user()->isAdmin())
            abort(403);
    }
}
