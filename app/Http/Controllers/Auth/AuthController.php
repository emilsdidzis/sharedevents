<?php

namespace App\Http\Controllers\Auth;

use Exception;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    /**
     * Redirect the user to the social authentication page.
     *
     * @return Response
     */
    public function redirectToProvider($social_channel)
    {
        return Socialite::driver($social_channel)->redirect();
    }
    public function showLogin()
    {
        return view('auth.login');
    }
    /**
     * Obtain the user information from social provider.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        try {
            $socialMediaUser = Socialite::driver($provider)->user();
        } catch (Exception $e) {
            return Redirect::to('auth/'.$provider);
        }

        $user = $this->findOrCreateUser($socialMediaUser, $provider);
        if (!$user instanceof(User::class))
        {
            return $user;
        }
        Auth::login($user, true);
        session()->regenerate();
        session(['locale' => $user->valoda]);
        return redirect()->intended(route('home'));
    }
    public function findOrCreateUser($socialMediaUser, $provider)
    {
        $user = User::where('social_provider_id', $socialMediaUser->getId())->where('social_provider', $provider)->first();
        $userPecEmail=User::where('email', '=', $socialMediaUser->getEmail())->first();
        if ($userPecEmail!=null)
        {
            if ($user!=null)
            {
                return $user;
            }
            if ($userPecEmail->social_provider==null)
            {
                $userPecEmail->social_provider=$provider;
                $userPecEmail->social_provider_id=$socialMediaUser->getId();
                $userPecEmail->save();
                return $userPecEmail;
            }
            return redirect(route('login'))->withErrors([
                'email' => t('This email is already associated with your %s account!', $userPecEmail->social_provider),
            ]);
        }
        else
        {
            if (is_null($user)) {
                $user = User::create([
                    'social_provider_id' => $socialMediaUser->getId(),
                    'social_provider' => $provider,
                    'name' => $socialMediaUser->getName(),
                    'email' => $socialMediaUser->getEmail(),
                    'email_verified_at' => Carbon::now(),
                    'avatar_url' => $socialMediaUser->getAvatar(),
                    'valoda' => session('locale', 'lv')
                ]);
            }
            return $user;
        }
    }
    
    /**
     * Destroy an authenticated session.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        Auth::guard('web')->logout();
        $valodaPirmsDestroy=session('locale', 'en');
        $request->session()->invalidate();

        $request->session()->regenerateToken();
        session(['locale' => $valodaPirmsDestroy]);
        return redirect('/');
    }
}
