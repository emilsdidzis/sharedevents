<?php

namespace App\Http\Controllers;

use App\Models\Notikums;
use App\Models\Komentars;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    public function addComment(Request $request, $id)
    {
        $notikums = Notikums::findOrFail($id);
        if (!$notikums->TgdPiederNotikumam() && !auth()->user()->isAdmin())
            abort(403);
        $rules = array(
            'saturs' => 'required|string|min:3|max:500',
        );
        $this->validate($request, $rules);
        $comment = new Komentars();
        $comment->saturs = $request->saturs;
        $comment->notikums_id = $id;
        $comment->autora_id = auth()->user()->id;
        $comment->save();
        return redirect()->route('event.view', [$id]);
    }
    public function deleteComment(Request $request)
    {
            $id = $request->id;
            $komentars = Komentars::findOrFail($id);
            if ($komentars->TgdIrAutors() || auth()->user()->isAdmin()) {
                $komentars->delete();
                return redirect()->back();
            }
            abort(403);
    }
}
