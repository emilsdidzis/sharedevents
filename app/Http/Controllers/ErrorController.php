<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ErrorController extends Controller
{
    public function showError(Request $request)
    {
        $errorMessage=$request->error;
        return view('error.error', compact(['errorMessage']));
    }
}
