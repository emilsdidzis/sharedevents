<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Grupa;
use App\Models\Notikums;
use App\Models\Komentars;
use App\Classes\EventButton;
use App\Http\Controllers\Webex\WebexMeetingController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Tio\Laravel\Facade as Translation;

class EventsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    //Atgriež masīvu ar pogas objektiem, kuriem ir nepieciešamā informācija
    //lai izveidotu notikuma pogas
    private function getButton(Notikums $notikums, $noformejums)
    {
        
        $buttons=[];
        if ($notikums->TgdIrAutors()==true || auth()->user()->isAdmin())
        {
            array_push($buttons, new EventButton(
                t('Edit'),
                route('event.edit', $notikums->id),
                $notikums->id,
                '',
                'btn-edit'
            ));
            array_push($buttons,new EventButton(
                t('Delete'),
                route('event.delete.ajax', $notikums->id),
                $notikums->id,
                '',
                'btn-delete'
            ));
        }
        if ($notikums->TgdIrAutors()==false)
        {
            array_push($buttons,new EventButton(
                t('Leave'),
                '',
                $notikums->id,
                '',
                'btn-leave'
            ));
        }
        if ($notikums->var_pieteikties)
        {
            array_push($buttons,new EventButton(
                t('Join'),
                '',
                $notikums->id,
                'j',
                'btn-join'. ($notikums->TgdIrPieteicies() ? ' hidden' : '')
            ));
            array_push($buttons,new EventButton(
                t('Unjoin'),
                '',
                $notikums->id,
                'u',
                'btn-join'. ($notikums->TgdIrPieteicies() ? '' : ' hidden')
            ));
        }
        array_push($buttons,new EventButton(
            t('View'),
            route('event.view', $notikums->id),
            $notikums->id,
            '',
            'btn-view'
        ));
        return $buttons;
    }
    //Atgriež katram notikumam nepieciešamo pogu masīvus.
    private function getButtons($notikumi, $noformejums)
    {
        $rezultats=[];
        foreach ($notikumi as $notikums)
        {
            $rezultats[$notikums->id]=$this->getButton($notikums, $noformejums);
        }
        return $rezultats;
    }
    //Atgriež gaidāmo notikumu skatu ar notikumiem
    public function index()
    {
        $salidzinamaisDatums = date("Y-m-d", strtotime(date("Y-m-d") . ' - 1 days'));
        $notikumi = auth()->user()->UseraNotikumi()->where('beigas', '>=', $salidzinamaisDatums)->orderBy('sakums', 'asc')->orderBy('beigas', 'asc')->get();
        $buttons=$this->getButtons($notikumi, 'card-btn');
        return view('event.events', compact('notikumi', 'buttons'));
    }
    //Atgriež visu notikumu saraksta skatu
    public function indexAll()
    {
        $notikumi = auth()->user()->UseraNotikumi()->orderBy('sakums', 'asc')->orderBy('beigas', 'asc')->get();
        $buttons=$this->getButtons($notikumi, 'card-btn');
        return view('event.events', compact('notikumi', 'buttons'));
    }
    //Atgriež notikuma rediģēšanas skatu, ja rediģē eksistējošu notikumu
    //tad tiek atgriezti arī notikuma dati
    public function edit($id = -1)
    {
        $notikums = Notikums::find($id);
        if ($id != -1) {
            $this->TgdIrAutorsVaiAdminsParbaude($notikums);
        }
        if ($id == -1) {
            $notikums = new Notikums();
            foreach ($notikums->all() as $kolonna) {
                $kolonna = NULL;
            }
        }
        return view('event.event_edit', compact('notikums', 'id'));
    }
    //Rediģēta vai jauna notikuma saglabāšana datu bāzē
    public function store(Request $request, $id)
    {
        //Request datu transformācija
        $Start = strToTime($request->sakums);
        $End = strToTime($request->beigas);
        $request->sakums = date(config('app.timeformat'), $Start);
        $request->beigas = date(config('app.timeformat'), $End);
        $request->var_pieteikties = $request->var_pieteikties == null ? false : true;
        $request->create_webex_meeting = $request->create_webex_meeting == null ? false : true;
        $rules = array(
            'virsraksts' => 'required|string|min:3|max:100',
            'saturs' => 'string|max:7500|nullable',
            'sakums' => 'required|date',
            'beigas' => 'required|date|after:sakums',
        );
        $this->validate($request, $rules);
        //Tiek izveidots jauns notikums ja id ir -1
        if ($id == -1) {
            $notikums = new Notikums();
            $notikums->autora_id = auth()->user()->id;
            $jauns = true;
        } else {
            $notikums = Notikums::findOrFail($id);
            if (!$notikums->tgdIrAutors() && !auth()->user()->isAdmin())
                abort(403);
            $jauns = false;
        }
        
        $notikums->virsraksts = $request->virsraksts;
        $notikums->saturs = $request->saturs;
        $notikums->sakums = $request->sakums;
        $notikums->beigas = $request->beigas;
        $notikums->var_pieteikties = $request->var_pieteikties;
        //WebexMeetingController::handleMeetingForEvent pārbauda vai ir nepieciešama Webex sapulce notikumam 
        $webexMeetingStatus = WebexMeetingController::handleMeetingForEvent($notikums, $request->create_webex_meeting);
        if ($webexMeetingStatus!=0)
        {
            return redirect()->back()->withErrors(['create_webex_meeting' => $webexMeetingStatus])->withInput();
        }
        $notikums->save();
        if ($jauns)
            $notikums->NotikumaUsers()->attach(auth()->user());
        return redirect()->route('event.view', [$notikums->id]);
    }
    //Funkcija saglabā lietotāja norādīto notikuma apmeklētību, un ja veiksmīgi izdevās
    //saglabāt apmeklētību, tad funkcija atgriež jauno apmeklētības vērtību
    public function joinOrUnjoin(Request $request)
    {
        $user = auth()->user();
        $notikums = Notikums::findOrFail($request->id);
        $NotikumaUser = $notikums->NotikumaUsers()->get()->where('id', $user->id)->first();
        if ($NotikumaUser != null) {
            $NotikumaUser = $NotikumaUser->pivot;
            if ($NotikumaUser->pieteicies == 1) {
                $NotikumaUser->pieteicies = 0;
                $NotikumaUser->save();
                return response()->json(['Pieteicies' => 0], 200);
            } else {
                $NotikumaUser->pieteicies = 1;
                $NotikumaUser->save();
                return response()->json(['Pieteicies' => 1], 200);
            }
        }
        return response()->json(['Error' => t('Could not find logged in user in event users!')], 403);
    }
    //Atgriež notikuma apskates skatu
    public function view($id)
    {
        $notikums = Notikums::findOrFail($id);
        $this->TgdIrAutorsVaiAdminsVaiDalibnieksParbaude($notikums);
        return view('event.event', compact(['notikums']));
    }
    //Dzēst notikumu dinamiski izmantojot AJAX pieprasījumu
    public function deleteAjax(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->id;
            $notikums = Notikums::findOrFail($id);
            if ($notikums->TgdIrAutors() || auth()->user()->isAdmin()) {
                $notikums->delete();
                return response()->json(['Id' => $id], 200);
            }
            return response()->json(['Error' => t('Logged in user is not author of the event!')], 403);
        }
    }
    //Dzēst notikumu vienkārši ar POST pieprasījumu
    public function delete(Request $request)
    {
            $id = $request->id;
            $notikums = Notikums::findOrFail($id);
            $this->TgdIrAutorsVaiAdminsParbaude($notikums);
            $notikums->delete();
            return redirect()->route('home');
    }
    //Notikuma pamešanas funkcija, noņem lietotāju no notikuma dalībnieku saraksta
    public function leave(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->id;
            $notikums = Notikums::findOrFail($id);
            if (!$notikums->TgdIrAutors() && $notikums->TgdPiederNotikumam()) {
                $notikums->NotikumaUsers()->detach(auth()->user());
                return response()->json(['Id' => $id], 200);
            }
            return response()->json(['Error' => t('Logged in user is author of the event or not involved in the event!')], 403);
        }
    }
    //Meklē pievienojamos lietotājus un grupas notikumam, bet no gala rezultāta tiek
    //noņemti jau pievienotie lietotāji un grupas, un lietotāji kuri ir bloķēti vai nobloķējuši
    public function searchUsersGroups(Request $request)
    {
        if ($request->ajax()) {
            if (strlen($request->search) < 3 || strlen($request->search) > 255) {
                return response()->json(['Users' => [], 'Grupas' => []], 200);
            }
            $notikums = Notikums::findOrFail($request->notikumsId);
            $this->TgdIrAutorsVaiAdminsParbaude($notikums);
            $notikumsUsers = $notikums->NotikumaUsers()->get()->pluck('id')->toArray();
            $notikumsGroups = $notikums->NotikumaGrupas()->get()->pluck('id')->toArray();
            $bloketaji = auth()->user()->NoblokejusieUsers()->get()->pluck('id')->toArray();
            $nobloketie = auth()->user()->BloketieUsers()->get()->pluck('id')->toArray();
            $UserMeklRez = User::where('name', 'LIKE', '%' . $request->search . "%")->whereNotIn('id', $notikumsUsers)->whereNotIn('id', $bloketaji)->whereNotIn('id', $nobloketie)->get();
            $grupaMeklRez = auth()->user()->UseraGrupas()->where('nosaukums', 'LIKE', '%' . $request->search . "%")->whereNotIn('grupa_id', $notikumsGroups)->get();
            return response()->json(['Users' => $UserMeklRez, 'Grupas' => $grupaMeklRez], 200);
        }
    }
    //Funkcijas, kas noņem grupu un tās dalībniekus no notikuma
    public function removeGroup(Request $request)
    {
        $grupa = Grupa::findOrFail($request->grupaId);
        $notikums = Notikums::findOrFail($request->notikumsId);
        $this->TgdIrAutorsVaiAdminsParbaude($notikums);
        if (!$notikums->NotikumaGrupas()->get()->contains($grupa))
            return response()->json(['Error' => t('Group was not found in Event')], 404);
        $notikums->NotikumaGrupas()->wherePivot('grupa_id', '=', $grupa->id)->detach();
        $nonemtie = [];
        foreach ($grupa->GrupasUsers()->get() as $user) {
            if ($notikums->NotikumaUsers()->get()->contains($user) && $notikums->autora_id != $user->id) {
                array_push($nonemtie, $user->id);
                $notikums->NotikumaUsers()->wherePivot('users_id', '=', $user->id)->detach();
            }
        }
        return response()->json([
            'nonemtie' => $nonemtie,
            'grupaId' => $grupa->id
        ], 200);
    }
    //Funkcija kas noņem lietotāju no dalībnieku saraksta
    public function removeUser(Request $request)
    {
        $user = User::findOrFail($request->userId);
        $notikums = Notikums::findOrFail($request->notikumsId);
        $this->TgdIrAutorsVaiAdminsParbaude($notikums);
        if (!$notikums->NotikumaUsers()->get()->contains($user))
            return response()->json(['Error' => t('User was not found in Event')], 404);
        if ($notikums->autora_id != $user->id)
            $notikums->NotikumaUsers()->detach($user);
        else
            return response()->json(['Error' => t('Can not remove user because user is author of the event!')], 403);
        return response()->json([
            'Nonemtais' => $user->id,
        ], 200);
    }
    //Funkcija, kas pievieno grupu un tās dalībniekus notikumam
    public function addGroup(Request $request)
    {
        $grupa = Grupa::findOrFail($request->grupaId);
        $notikums = Notikums::findOrFail($request->notikumsId);
        $this->TgdIrAutorsVaiAdminsParbaude($notikums);
        if (!$notikums->NotikumaGrupas()->get()->contains($grupa)) {
            $notikums->NotikumaGrupas()->attach($grupa);
            $pievienotie = [];
            foreach($grupa->GrupasUsers()->get() as $user)
            {
                if (!auth()->user()->NoblokejusieUsers()->get()->contains($user) && !$notikums->NotikumaUsers()->get()->contains($user))
                {
                    array_push($pievienotie, $user);
                    $notikums->NotikumaUsers()->attach($user);
                }
            }
            return response()->json([
                'grupaId' => $grupa->id,
                'nosaukums' => $grupa->nosaukums,
                'Users' => $pievienotie
            ], 200);
        }
        return response()->json([
            'grupaId' => $grupa->id,
            'nosaukums' => $grupa->nosaukums,
            'Users' => null
        ], 403);
    }
    //Funkcija, kas pievieno lietotāju notikumam
    public function addUser(Request $request)
    {
        $user = User::findOrFail($request->userId);
        $notikums = Notikums::findOrFail($request->notikumsId);
        $this->TgdIrAutorsVaiAdminsParbaude($notikums);
        if (
            (!auth()->user()->NoblokejusieUsers()->get()->contains($user) &&
            !$notikums->NotikumaUsers()->get()->contains($user)
            ) ||
            auth()->user()->auth()->user()->isAdmin()
        ) {
            $notikums->NotikumaUsers()->attach($user);
            return response()->json([
                'user' => [$user],
            ], 200);
        }
        return response()->json([
            'user' => null
        ], 403);
    }
    //Funkcija kas pārbauda vai pieslēdzies lietotājs ir administrators
    //vai autors notikumam
    private function TgdIrAutorsVaiAdminsParbaude(Notikums $notikums)
    {
        if (!$notikums->TgdIrAutors() && !auth()->user()->isAdmin())
            abort(403);
    }
    //Funkcija kas pārbauda vai pieslēdzies lietotājs ir administrators
    //vai autors notikumam vai dalībnieks notikumam
    private function TgdIrAutorsVaiAdminsVaiDalibnieksParbaude(Notikums $notikums)
    {
        if (!$notikums->TgdIrAutors() && !auth()->user()->isAdmin() && !$notikums->TgdPiederNotikumam())
            abort(403);
    }
}
