<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class CalendarController extends Controller
{
    ///https://element.eleme.io/#/en-US/component/date-picker#other-measurements
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    private function getGaduNoNotikumiem($notikumi, $pecSakuma)
    {
        $notikums=$notikumi->first();
        $datums=$notikums ? Carbon::create($pecSakuma ? $notikums->sakums : $notikums->beigas) :Carbon::create(null,null,null);
        return $datums->year;
    }
    private function getMinimaloGadu()
    {
        $notikumi= auth()->user()->UseraNotikumi()->orderBy('sakums')->get();
        return $this->getGaduNoNotikumiem($notikumi, true);
    }
    private function getMaksimaloGadu()
    {
        $notikumi= auth()->user()->UseraNotikumi()->orderByDesc('beigas')->get();
        return $this->getGaduNoNotikumiem($notikumi, false);
    }
    private function nedelasDienas(){
        return [
        t('Monday'),
        t('Tuesday'),
        t('Wednesday'),
        t('Thursday'),
        t('Friday'),
        t('Saturday'),
        t('Sunday')
        ];
    }
    private $krasas=[
        '#B40000', //Sarkans
        '#E77300', //Oranžš
        '#0B9100', //Zaļš
        '#008B91', //Zils
        '#730082', //Violets
        '#5D332E', //Brūns
    ];
    private $tukssNotikums=[
        'kartasNr' =>-1,
        'id'=>-1,
        'virsraksts' =>'',
        'krasa' =>'transparent' //#FFFFFF - production #000000 - debug
    ];
    private function GetNotikumaKrasu($notikumaId, &$notikumuKrasas)
    {
        $krasuSkaits=sizeof($this->krasas);
        if (array_key_exists($notikumaId, $notikumuKrasas))
            return $notikumuKrasas[$notikumaId];
        $nakamaKrasa=sizeof($notikumuKrasas)%$krasuSkaits;
        if ($nakamaKrasa==$krasuSkaits)
            $nakamaKrasa=0;
        $notikumuKrasas[$notikumaId]=$this->krasas[$nakamaKrasa];
        return $this->krasas[$nakamaKrasa];
    }
    private function GetRobezas($datums, $pieskaititMenesi=0)
    {
        $datums=Carbon::create($datums)->addMonths($pieskaititMenesi);
        $sakums=Carbon::create($datums)->startOfMonth()->startOfWeek();
        $beigas=Carbon::create($datums)->endOfMonth()->endOfWeek();
        return [
            'datums'=>$datums,
            'sakums'=>$sakums,
            'beigas'=>$beigas,
            'nedelas'=>$sakums->diffInWeeks($beigas)+1
        ];
    }
    private function GetNedelasRobezas($datums)
    {
        $datums=Carbon::Create($datums);
        $sakums=$datums->copy()-> startOfWeek();
        $beigas=$datums->copy()->endOfWeek();
        return [
            'sakums'=>$sakums,
            'beigas'=>$beigas
        ];
    }
    private function GetNedelasNotikumus($nedelasRobezas)
    {
        $nedelasNotikumi=auth()->user()->UseraNotikumi()
            ->where(function ($query) use($nedelasRobezas){
                $query->orWhere(function($query) use($nedelasRobezas){
                    $query->where('sakums', '<=', $nedelasRobezas['sakums'])
                    ->where('beigas', '>=', $nedelasRobezas['beigas']);
                })
                ->orWhere(function($query) use($nedelasRobezas){
                    $query->where('beigas', '>=', $nedelasRobezas['sakums'])
                    ->where('beigas', '<=', $nedelasRobezas['beigas']);
                })
                ->orWhere(function($query) use($nedelasRobezas){
                    $query->where('sakums', '>=', $nedelasRobezas['sakums'])
                    ->where('sakums', '<=', $nedelasRobezas['beigas'])
                ;
                });
            })->orderBy('sakums')->get();
        return $nedelasNotikumi;
        
    }
    private function GetKartasNr($notikumi)
    {
        if (empty($notikumi))
            return 0;
        $notikumi=collect($notikumi)->sortBy('kartasNr');
        $i=0;
        foreach ($notikumi as $notikums)
        {
            if ($i!=$notikums['kartasNr'])
            {
                return $i;
            }
            $i++;
        }
        return $notikumi->pluck('kartasNr')->max()+1;
    }
    private function aizpilditTuksumusKalendara($nedelasDienas, $lielakaisKartasNr)
    {
        foreach($nedelasDienas as $diena)
        {
            $tuksieNotikumi=[];
            $arrayPos=0;
            $diena['notikumi']=collect($diena['notikumi'])->sortBy('kartasNr');
            for ($i=0;$i<$lielakaisKartasNr;$i++)
            {
                if ($arrayPos<sizeof($diena['notikumi']) && $i==$diena['notikumi']->values()->get($arrayPos)['kartasNr'])
                {
                    $arrayPos++;
                }
                else
                {
                    $tukssNotikums=$this->tukssNotikums;
                    $tukssNotikums['kartasNr']=$i;
                    array_push($tuksieNotikumi, $tukssNotikums);
                }
            }
            foreach($tuksieNotikumi as $tukssNotikums)
            {
                $diena['notikumi']=collect($diena['notikumi']);
                $diena['notikumi']->push($tukssNotikums);
            }
            $diena['notikumi']=$diena['notikumi']->sortBy('kartasNr');
        }    
    }
    public function view(Request $request, $pieskaititMenesi=0)
    {
        $datums=null;
        if ($request->year!=null && strlen($request->year)>1 &&
            $request->month!=null && strlen($request->month)>0)
        {
            $datums=$request->year.'-'.$request->month.'-01';
        }
        $robezas=strtotime($datums) ? $this->GetRobezas($datums, $pieskaititMenesi) : $this->GetRobezas(date("Y-m-d"), $pieskaititMenesi);
        $notikumuKrasas=[];
        $tagadDiena=$robezas['sakums'];
        $izveletaisGads=$robezas['datums']->copy()->format('Y');//$tagadDiena->copy()->year.'-'.$tagadDiena->copy()->month;
        $izveletaisMenesis=$robezas['datums']->copy()->month;
        $menesis=[];
        for ($i=0;$i<$robezas['nedelas'];$i++)
        {
            $nedela = [
                'dienas' =>[],
                'lielakaisKartasNr' =>0
            ];
            $kartasNr =[];
            $nedelasRobezas=$this->GetNedelasRobezas($tagadDiena);
            $nedelasNotikumi=$this->GetNedelasNotikumus($nedelasRobezas);
            $notikumaKartasNr=[];
            $lielakaisKartasNr=0;
            for ($j=0;$j<7;$j++)
            {
                $diena=[
                    'nr'=>$tagadDiena->day,
                    'aktualaisMenesis'=>$tagadDiena->month==$robezas['datums']->month,
                    'notikumi' =>[],
                ];
                foreach ($nedelasNotikumi as $notikums)
                {
                    $sakums=Carbon::create($notikums->sakums);
                    $beigas=Carbon::create($notikums->beigas);
                    $virsraksts='';
                    $kartasNr=0;
                    if ($sakums>=$tagadDiena->copy()->addDay() || $beigas<$tagadDiena)
                        continue;
                    if (($sakums->month==$tagadDiena->month && $sakums->day==$tagadDiena->day) || $j==0)
                    {
                        $kartasNr=$this->GetKartasNr($diena['notikumi']);
                        $virsraksts=$notikums->virsraksts;
                        $notikumaKartasNr[$notikums->id]=$kartasNr;
                    }
                    else
                    {
                        $kartasNr=$notikumaKartasNr[$notikums->id];
                    }
                    $lielakaisKartasNr=max([$lielakaisKartasNr, (1+$kartasNr)]);
                    array_push($diena['notikumi'], [
                        'kartasNr' =>$kartasNr,
                        'id'=>$notikums->id,
                        'virsraksts' =>$virsraksts,
                        'krasa' =>$this->GetNotikumaKrasu($notikums->id, $notikumuKrasas)
                    ]);
                }
                array_push($nedela['dienas'], collect($diena));
                $tagadDiena=$tagadDiena->addDay();
            }
            $nedela['lielakaisKartasNr']=$lielakaisKartasNr;
            $nedela['dienas']=collect($nedela['dienas']);
            $this->aizpilditTuksumusKalendara($nedela['dienas'], $lielakaisKartasNr);
            $nedela=collect($nedela);
            array_push($menesis, $nedela);
        }
        $menesis=json_decode(collect($menesis)->toJson());
        //dump($menesis);
        $nedelasDienas=$this->nedelasDienas();
        $minYear=$this->getMinimaloGadu();
        $maxYear=$this->getMaksimaloGadu();
        $yearMonthPickerData=[
            'izveletaisGads' => $izveletaisGads,
            'izveletaisMenesis' => $izveletaisMenesis,
            'minYear' => $minYear,
            'maxYear' => $maxYear
        ];
        return view('calendar.calendar', compact(['menesis', 'nedelasDienas', 'yearMonthPickerData']));
    }
    public function viewNextMonth(Request $request)
    {
        return $this->view($request, 1);
    }
    public function viewPrevMonth(Request $request)
    {
        return $this->view($request, -1);
    }
}
