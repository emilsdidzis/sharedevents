<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Komentars extends Model
{
    use HasFactory;
    protected $table = 'komentars';
    public function Notikums()
    {
        return $this->belongsTo(Notikums::class, 'notikums_id', 'id');
    }
    public function Autors()
    {
        return $this->belongsTo(User::class, 'autora_id', 'id');
    }
    public function TgdIrAutors()
    {
        //dump($this->Autors());
        if ($this->Autors()->get()[0]==auth()->user())
            return true;
        return false;
    }
}
