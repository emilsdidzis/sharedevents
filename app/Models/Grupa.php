<?php

namespace App\Models;

use App\Models\Notikums;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Grupa extends Model
{
    use HasFactory;
    protected $table = 'grupa';
    public function GrupasNotikumi()
    {
        return $this->belongsToMany(Notikums::class, 'notikuma_grupas', 'grupa_id', 'notikums_id');
    }
    public function GrupasUsers()
    {
        return $this->belongsToMany(User::class, 'piederiba_grupai', 'grupa_id', 'users_id');
    }
    public function BloketieUsers()
    {
        return $this->belongsToMany(User::class, 'bloketas_grupas', 'bloketa_grupa', 'bloketajs');
    }
    public function Autors()
    {
        return $this->hasOne(User::class, 'id', 'autora_id');
    }
    public function TgdIrAutors()
    {
        if ($this->Autors()->get()[0]->id==auth()->user()->id)
            return true;
        return false;
    }
    public function TgdPiederGrupai()
    {
        return $this->GrupasUsers()->get()->contains(auth()->user());
    }
    public function TgdBlokeja()
    {
        return $this->BloketieUsers()->get()->contains(auth()->user());
    }
    public static function boot() {
        parent::boot();

        static::deleting(function($grupa) { // before delete() method call this
             //$notikums->NotikumaUsers()->pivot->delete();

             //$notikums->NotikumaGrupas()->pivot->delete();
             //$notikums->Komentari()->delete();
             foreach ($grupa->GrupasUsers()->get() as $GrupasUser)
             {
                 $GrupasUser->pivot->delete();
             }
             foreach ($grupa->GrupasNotikumi()->get() as $GrupasNotikums)
             {
                 $GrupasNotikums->pivot->delete();
             }
             foreach ($grupa->BloketieUsers()->get() as $BloketaisUser)
             {
                 $BloketaisUser->pivot->delete();
             }
        });
    }
}
