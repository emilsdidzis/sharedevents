<?php

namespace App\Models;

use App\Http\Controllers\GroupController;
use App\Models\Grupa;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'social_provider_id',
        'social_provider',
        'name',
        'email',
        'email_verified_at',
        'password',
        'avatar_url',
        'valoda'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'social_provider_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    // determine if User is admin by checking DB field isAdmin (1 is admin)
    public function isAdmin() {
        return ($this->is_admin == 1);
    }
    public function UseraNotikumi()
    {
        return $this->belongsToMany(Notikums::class, 'notikuma_lietotaji', 'users_id', 'notikums_id')->withPivot('pieteicies', 'pazinots');
    }
    public function UserIzveidotieNotikumi()
    {
        return $this->hasMany(Notikums::class, 'autora_id', 'id');
    }
    public function BloketasGrupas()
    {
        return $this->belongsToMany(Grupa::class, 'bloketas_grupas', 'bloketajs', 'bloketa_grupa');
    }
    public function UseraGrupas()
    {
        return $this->belongsToMany(Grupa::class, 'piederiba_grupai', 'users_id', 'grupa_id');
    }
    public function UserIzveidotasGrupas()
    {
        return $this->hasMany(Grupa::class, 'autora_id', 'id');
    }
    public function BloketieUsers()
    {
        return $this->belongsToMany(User::class, 'bloketie_lietotaji', 'bloketajs', 'blokejamais');
    }
    public function NoblokejusieUsers()
    {
        return $this->belongsToMany(User::class, 'bloketie_lietotaji', 'blokejamais', 'bloketajs');
    }
    public function Komentari()
    {
        return $this->hasMany(Komentars::class, 'autora_id', 'id');
    }
    public function TgdIrNoblokejis()
    {
        return $this->NoblokejusieUsers()->get()->contains(auth()->user());
    }
    public function WebexTokens()
    {
        return $this->hasOne(WebexUserTokens::class, 'user_id', 'id');
    }
    public static function boot() {
        parent::boot();

        static::deleting(function($user) {
            foreach ($user->UserIzveidotasGrupas()->get() as $grupa)
            {
                $grupa->delete();
            }
            foreach ($user->Komentari()->get() as $komentars)
            {
                $komentars->delete();
            }
            foreach ($user->UserIzveidotieNotikumi()->get() as $notikums)
            {
                $notikums->delete();
            }
            foreach ($user->UseraNotikumi()->get() as $notikums)
            {
                $notikums->pivot->delete();
            }
            foreach ($user->BloketasGrupas()->get() as $grupa)
            {
                $grupa->pivot->delete();
            }
            foreach ($user->UseraGrupas()->get() as $grupa)
            {
                $grupa->pivot->delete();
            }
            foreach ($user->BloketieUsers()->get() as $user)
            {
                $user->pivot->delete();
            }
            foreach ($user->NoblokejusieUsers()->get() as $user)
            {
                $user->pivot->delete();
            }
            foreach ($user->WebexTokens()->get() as $webextokens)
            {
                $webextokens->delete();
            }
        });
    }
}
