<?php

namespace App\Models;

use App\Models\Notikums;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class WebexMeeting extends Model
{
    use HasFactory;
    protected $table = 'webex_meeting';
    public function Notikums()
    {
        return $this->hasOne(Notikums::class, 'webex_meeting_id', 'id');
    }
}
