<?php

namespace App\Models;

use App\Http\Controllers\Webex\WebexMeetingController;
use Exception;
use App\Models\User;
use App\Models\Grupa;
use App\Models\WebexMeeting;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Notikums extends Model
{
    use HasFactory;
    protected $fillable = [
        'virsraksts',
        'saturs',
        'sakums',
        'beigas',
        'var_pieteikties'
    ];
    public function NotikumaUsers()
    {
        return $this->belongsToMany(User::class, 'notikuma_lietotaji', 'notikums_id', 'users_id')->withPivot(['pieteicies', 'pazinots']);
    }
    public function NotikumaGrupas()
    {
        return $this->belongsToMany(Grupa::class, 'notikuma_grupas', 'notikums_id', 'grupa_id');
    }
    public function Autors()
    {
        return $this->belongsTo(User::class, 'autora_id', 'id');
    }
    public function TgdIrAutors()
    {
        //dump($this->Autors());
        if ($this->Autors()->get()[0]==auth()->user())
            return true;
        return false;
    }
    public function TgdIrPieteicies()
    {
        $NotikumaUser=$this->NotikumaUsers()->get()->where('id', auth()->user()->id)->first();
        if ($NotikumaUser!=null)
        {
            try {
                $pieteicies = $NotikumaUser->relations['pivot']->pieteicies;
                if ($pieteicies==1)
                    return true;
                return false;
            }
            catch(Exception $e)
            {
                return false;
            }
            
        }
        return false;
    }
    public function TgdPiederNotikumam()
    {
        return $this->NotikumaUsers()->get()->contains(auth()->user());
    }
    public function Komentari()
    {
        return $this->hasMany(Komentars::class, 'notikums_id', 'id');
    }
    public function WebexMeeting()
    {
        return $this->hasOne(WebexMeeting::class, 'id', 'webex_meeting_id');
    }
    public static function boot() {
        parent::boot();

        static::deleting(function($notikums) { // before delete() method call this
             //$notikums->NotikumaUsers()->pivot->delete();
             
             //$notikums->NotikumaGrupas()->pivot->delete();
             //$notikums->Komentari()->delete();
             WebexMeetingController::deleteWebexMeetingFromEvent($notikums);
             foreach ($notikums->NotikumaUsers()->get() as $NotikumaUser)
             {
                 $NotikumaUser->pivot->delete();
             }
             foreach ($notikums->NotikumaGrupas()->get() as $NotikumaGrupa)
             {
                 $NotikumaGrupa->pivot->delete();
             }
             foreach ($notikums->Komentari()->get() as $Komentars)
             {
                 $Komentars->delete();
             }
        });
    }
}
