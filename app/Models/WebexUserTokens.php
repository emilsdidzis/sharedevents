<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WebexUserTokens extends Model
{
    use HasFactory;
    protected $table = 'webex_user_tokens';
    public function User()
    {
        return $this->hasOne(WebexUserTokens::class, 'id', 'user_id');
    }
}
