<?php

namespace App\View\Components;

use Illuminate\View\Component;

class NavigationLayout extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $navLinks=[];
    public function __construct()
    {
        if (auth()->user()!=null)
        {
            array_push($this->navLinks, [
                'href' => route('home'),
                'active' => request()->routeIs('home'),
                'text' => t('Upcoming events'),
            ]);
            array_push($this->navLinks, [
                'href' => route('calendar.view'),
                'active' => request()->routeIs('calendar.view'),
                'text' => t('Calendar'),
            ]);
            array_push($this->navLinks, [
                'href' => route('group.index'),
                'active' => request()->routeIs('group.index'),
                'text' => t('My groups'),
            ]);
            array_push($this->navLinks, [
                'href' => route('user.index'),
                'active' => request()->routeIs('user.index'),
                'text' => t('Users'),
            ]);
            array_push($this->navLinks, [
                'href' => route('event.all'),
                'active' => request()->routeIs('event.all'),
                'text' => t('All my events'),
            ]);
        }
        else
        {
            array_push($this->navLinks, [
                'href' => route('login'),
                'active' => request()->routeIs('login'),
                'text' => t('Log in'),
            ]);
            array_push($this->navLinks, [
                'href' => route('register'),
                'active' => request()->routeIs('register'),
                'text' => t('Register'),
            ]);
        }
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $navLinks=$this->navLinks;
        return view('layouts.navigation', compact(['navLinks']));
    }
}
