<?php

namespace App\View\Components;

use Illuminate\View\Component;

class YearMonthPicker extends Component
{
    private function menesi()
    {
        return [
            1 => t('January'),
            2 => t('February'),
            3 => t('March'),
            4 => t('April'),
            5 => t('May'),
            6 => t('June'),
            7 => t('July'),
            8 => t('August'),
            9 => t('September'),
            10 => t('October'),
            11 => t('November'),
            12 => t('December')
        ];
    }
    public $selectedYear;
    public $selectedMonth;
    public $minYear;
    public $maxYear;
    public $prevUrl;
    public $nextUrl;
    public $postUrl;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($selectedYear, $selectedMonth, $minYear, $maxYear, $prevUrl, $nextUrl, $postUrl)
    {
        $this->selectedYear=$selectedYear;
        $this->selectedMonth=$selectedMonth;
        $this->minYear=$minYear;
        $this->maxYear=$maxYear;
        $this->prevUrl=$prevUrl;
        $this->nextUrl=$nextUrl;
        $this->postUrl=$postUrl;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $menesi=$this->menesi();
        $gadi=[];
        for ($i=min($this->minYear, $this->selectedYear);$i<=max($this->maxYear, $this->selectedYear);$i++)
        {
            array_push($gadi, $i);
        }
        return view('components.year-month-picker', compact(['menesi', 'gadi']));
    }
}
