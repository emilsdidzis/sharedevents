<?php

namespace App\View\Components;

use Carbon\Carbon;
use Illuminate\View\Component;

class EventCard extends Component
{
    public $notikums;
    public $buttons;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($notikums, $buttons)
    {
        $sakums=Carbon::create($notikums->sakums);
        $beigas=Carbon::create($notikums->beigas);
        $beigas=$sakums->toDateString()==$beigas->toDateString() ? $beigas->toTimeString('minute') : $beigas->toDateTimeString('minute');
        $notikums->sakums=$sakums->toDateTimeString('minute');
        $notikums->beigas=$beigas;
        $this->notikums=$notikums;
        $this->buttons=$buttons;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.event-card');
    }
}
