<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use Laravel\Socialite\Facades\Socialite;
use App\Http\Controllers\ErrorController;
use App\Http\Controllers\GroupController;
use App\Http\Controllers\EventsController;
use App\Http\Controllers\UploadController;
use App\Http\Controllers\CalendarController;
use App\Http\Controllers\CommentsController;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Webex\WebexAuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require __DIR__ . '/auth.php';
//Route::get('/phpinfo', function(){return phpinfo();});
Route::middleware('set.locale')->group(function () {
    Route::middleware(['auth', 'verified'])->group(function () {
        Route::redirect('/home', '/');
        Route::get('/',  [EventsController::class, 'index'])->name('home');
        Route::get('/event/create', [EventsController::class, 'edit'])->name('event.create');
        Route::get('/event/all', [EventsController::class, 'indexAll'])->name('event.all');
        Route::get('/event/edit/{id}', [EventsController::class, 'edit'])->name('event.edit');
        Route::get('/event/view/{id}', [EventsController::class, 'view'])->name('event.view');
        Route::post('/event/store/{id}', [EventsController::class, 'store'])->name('event.store');
        Route::post('/event/join', [EventsController::class, 'joinOrUnjoin'])->name('event.join');
        Route::post('/event/add/comment/{id}', [CommentsController::class, 'addComment'])->name('comment.add');
        Route::post('/event/delete/comment/{id}', [CommentsController::class, 'deleteComment'])->name('comment.delete');
        Route::post('/event/deleteajax', [EventsController::class, 'deleteAjax'])->name('event.delete.ajax');
        Route::post('/event/delete/{id}', [EventsController::class, 'delete'])->name('event.delete');
        Route::post('/event/leave', [EventsController::class, 'leave'])->name('event.leave');
        Route::post('/event/remove/group', [EventsController::class, 'removeGroup'])->name('event.remove.group');
        Route::post('/event/remove/user', [EventsController::class, 'removeUser'])->name('event.remove.user');
        Route::post('/event/add/user', [EventsController::class, 'addUser'])->name('event.add.user');
        Route::post('/event/add/group', [EventsController::class, 'addGroup'])->name('event.add.group');
        Route::post('/event/search/usersandgroups', [EventsController::class, 'searchUsersGroups'])->name('event.search.usersAndGroups');

        Route::get('/groups', [GroupController::class, 'index'])->name('group.index');
        Route::get('/group/view/{id}', [GroupController::class, 'view'])->name('group.view');
        Route::get('/group/create', [GroupController::class, 'edit'])->name('group.create');
        Route::get('/group/edit/{id}', [GroupController::class, 'edit'])->name('group.edit');
        Route::post('/group/leave/{id}', [GroupController::class, 'leave'])->name('group.leave');
        Route::post('/group/store/{id}', [GroupController::class, 'store'])->name('group.store');
        Route::post('/group/deleteajax', [GroupController::class, 'deleteAjax'])->name('group.delete.ajax');
        Route::post('/group/delete/{id}', [GroupController::class, 'delete'])->name('group.delete');
        Route::post('/group/block', [GroupController::class, 'block'])->name('group.block');
        Route::post('/group/remove/user', [GroupController::class, 'removeUser'])->name('group.remove.user');
        Route::post('/group/search/usersandgroups', [GroupController::class, 'searchUsers'])->name('group.search.users');
        Route::post('/group/add/user', [GroupController::class, 'addUser'])->name('group.add.user');

        Route::get('/user/view/{id}', [UserController::class, 'view'])->name('user.view');
        Route::get('/users', [UserController::class, 'index'])->name('user.index');
        Route::get('/user/self', [UserController::class, 'userSelfProfile'])->name('user.self.profile');
        Route::get('/user/edit/{id}', [UserController::class, 'edit'])->name('user.edit');
        Route::post('/user/search', [UserController::class, 'search'])->name('user.search');
        Route::post('/user/delete/{id}', [UserController::class, 'delete'])->name('user.delete');
        Route::post('/user/block', [UserController::class, 'block'])->name('user.block');
        Route::post('/user/store/{id}', [UserController::class, 'store'])->name('user.store');

        Route::get('/calendar/view', [CalendarController::class, 'view'])->name('calendar.view');
        Route::get('/calendar/view/next', [CalendarController::class, 'viewNextMonth'])->name('calendar.view.next');
        Route::get('/calendar/view/prev', [CalendarController::class, 'viewPrevMonth'])->name('calendar.view.prev');

        Route::post('/ckeupload', [UploadController::class, 'CKEUpload'])->name('CKEUpload');
        Route::post('/ajaxupload', [UploadController::class, 'ajaxUpload'])->name('ajaxUpload');


        Route::get('/webex/auth', [WebexAuthController::class, 'redirectToWebexAuth'])->name('webex.auth');
        Route::get('/webex/callback', [WebexAuthController::class, 'handleCallback'])->name('webex.callback');

    });
    Route::post('/language/{valoda}', [LanguageController::class, 'setLanguage'])->name('setLanguage');


    Route::get('/error', [ErrorController::class, 'showError'])->name('error');

    //Legālās lapas
    Route::view('/privacy-policy', 'legal/privacy-policy');
    Route::view('/data-deletion', 'legal/data-deletion');
    Route::view('/terms-of-service', 'legal/terms-of-service');
});
