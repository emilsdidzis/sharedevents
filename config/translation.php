<?php

return [
    'key' => env('TRANSLATION_KEY'),
    'source_locale' => 'en',
    'target_locales' => ['lv'],

    /* Directories to scan for Gettext strings */
    'gettext_parse_paths' => ['app', 'resources'],

    /* Where the Gettext translations are stored */
    'gettext_locales_path' => 'resources/lang/gettext'
];