Jāsalabo:
1. Navigācija telefona izmērā
2. Join, unjoin poga notikuma skatā
3. Iespēja dzēst lietotāju, gan admindam, gan pašam lietotājam
`
Papildinājumi:
1. Zoom api: https://marketplace.zoom.us/develop/create Iespējams Oauth variants
2. Kalendārs, kurā iezīmēti notikumi
3. Grupas kurām var jebkurš pievienoties
4. Notikumu filtri
5. Lietotājus varbūt meklēt pēc kaut kāda unikāla Id, lai neapjūk gadījumos ar cilvēkiem kam vienādi vārdi un uzvārdi.
6. Atļaujas - grupu notikumam var pievienot tikai autors, notikumam var pievienot tikai apstiprinātos lietotājus jeb draugus
7. Iespēja apskatīties citu cilvēku ieplānotos notikumus, bet tad ir nepieciešami papildus iestatījumi profilam un/vai notikumam, lai norādītu ka notikums ir privāts.
8. Iespēja pievienot kategorijas notikumiem un varbūt norādīt notikuma atļaujas caur kategorijām.
9. Notikumi, kas atkārtojas(Ko darīt ar čatu utml.)
10. Kopīgojami linki uz notikumiem
