<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{t('My groups')}}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200" id="grouplist">
                    <a href="{{route('group.create')}}">
                        <button
                            class="ml-4 bg-blue-600 px-5 py-3 text-sm shadow-sm font-bold tracking-wider border text-white rounded-md hover:shadow-lg hover:bg-blue-700">
                            {{t('Create group')}}</button>
                    </a>
                    @foreach($grupas as $grupa)
                    <x-group-card :grupa=$grupa />
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
        checkGroupCount();

        function checkGroupCount() {
            if (!$('div[group-id-galvenais]').length) {
                $('#grouplist').append('{{t("You do not have any groups")}}');
            }
        }

        $(".btn-delete").on('click', function (e) {
          var url = "{{ route('group.delete.ajax') }}";
          var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
          var btn = $(this);
          var grupaId=btn.attr('group-id');
          e.preventDefault();
          //console.log(btn.attr('group-id'));
          $.ajax({
              type: "POST",
              url: url,
              data: {id: btn.attr('group-id'), _token: CSRF_TOKEN},
              success: function (data) {

                  $('div[group-id-galvenais="' + data['Id'] +'"]').remove();

                  checkGroupCount();
              },
              error: function (data) {
                  console.log('Error:', data);
              }
          });
      });
    });
    </script>
</x-app-layout>
