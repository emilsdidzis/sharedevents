<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ t('Edit group') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form method="POST" action="{{route('group.store', ['id' => $id])}}">
                        @csrf
                        <x-validation-error class="mb-4" :errors="$errors" title="nosaukums"/>
                        <x-inputv2 name="nosaukums" value="{{$grupa->nosaukums}}" title="{{t('Group name')}}" />
                            <br/>
                        <div class="flex p-1">
                            <button role="submit" class="mb-4 p-3 bg-blue-500 text-white hover:bg-blue-400"
                                required>{{t('Save')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


</x-app-layout>
