<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ $grupa->nosaukums }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                @if ($grupa->TgdIrAutors()==true || auth()->user()->IsAdmin())
                <div class="inline-flex">
                    <a href="{{url('group/edit/'.$grupa->id)}}"
                    class="px-4 py-2 bg-gray-500 shadow-lg border rounded-lg text-white uppercase font-semibold tracking-wider focus:outline-none focus:shadow-outline hover:bg-teal-400 active:bg-teal-400">
                    {{t('Edit')}}</a>
                    <form method="POST" action="{{ route('group.delete', $grupa->id) }}" >
                    @csrf
                    <button type="submit" onclick="event.preventDefault();this.closest('form').submit();"
                    class="btn-delete px-4 py-2 bg-gray-500 shadow-lg border rounded-lg text-white uppercase font-semibold tracking-wider focus:outline-none focus:shadow-outline hover:bg-teal-400 active:bg-teal-400">
                    {{t('Delete')}}</button>
                    </form>
                </div>
                    @endif
                    <div>
                        <p id="useruList"><strong>{{t('Group users: ')}}</strong>
                            @foreach($grupa->GrupasUsers()->get() as $user)
                            <span class="user mr-4" user-id="{{$user->id}}">
                            <a href="{{route('user.view', [$user->id])}}">
                                {{$user->name}}
                                </a>
                                @if(($grupa->TgdIrAutors() || auth()->user()->isAdmin()) && $grupa->autora_id!=$user->id)
                                <svg xmlns="http://www.w3.org/2000/svg" class="remove-user h-5 w-5 inline"
                                    viewBox="0 0 20 20" fill="#c53030" user-id="{{$user->id}}">
                                    <path fill-rule="evenodd"
                                        d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                                        clip-rule="evenodd" />
                                </svg>
                                @endif
                            </span>
                            @endforeach
                        </p>
                        @if($grupa->TgdIrAutors() || auth()->user()->isAdmin())
                        <x-searchUsers :grupa=$grupa/>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
        $("#useruList").on('click', '.remove-user', function(){
                var url = "{{ route('group.remove.user') }}";
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                var btn = $(this);
                var userId=btn.attr('user-id');
                //console.log(btn.attr('user-id'));
                $.ajax({
                type: "POST",
                 url: url,
                data: {grupaId: {{$grupa->id}},userId: btn.attr('user-id'), _token: CSRF_TOKEN},
                success: function (data) {

                  $('span[user-id="' + data['Nonemtais'] +'"]').remove();
                },
                error: function (data) {
                  console.log('Error:', data);
                }
                });
            });
        });

        function addUserToList(useri)
        {
            for (let i in useri)
            {
                //console.log(useri[i]);
                var elem= $.parseHTML($.trim(`<span class='user mr-4' >
                            <a>

                                </a>
                                @if($grupa->TgdIrAutors() || auth()->user()->isAdmin())
                                <svg xmlns='http://www.w3.org/2000/svg' class='remove-user h-5 w-5 inline'
                                    viewBox='0 0 20 20' fill='#c53030'>
                                    <path fill-rule='evenodd'
                                        d='M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z'
                                        clip-rule='evenodd' />
                                </svg>
                                @endif
                            </span>`));
                $(elem).attr('user-id', useri[i].id);
                $(elem).find("svg").attr('user-id', useri[i].id);
                $(elem).find("a").attr('href', '/user/view/' + useri[i].id);
                $(elem).find("a").text(useri[i].name);
                $("#useruList").append(elem);
            }
        }
    </script>
    <style>
        .remove-user:hover {
            cursor: pointer;
        }
        </style>
</x-app-layout>
