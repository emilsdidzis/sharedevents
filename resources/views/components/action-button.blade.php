<button
{{ $attributes->merge(['type' => 'submit',
'class' => '
ml-4 
bg-blue-600 
px-5 
py-3 
text-sm 
shadow-sm 
font-bold 
tracking-wider 
border 
text-white 
rounded-md 
hover:shadow-lg 
hover:bg-blue-700']) }}>
    {{$slot}}</button>