@props(['notikums'])
<div>
    <h1><strong>{{t('Add users and groups')}}</strong></h1>
    <div class="inline-flex flex-col justify-center relative text-gray-500">
        <div class="relative">
            <input type="text" id="searchUsersAndGroups"
                class="p-2 pl-8 rounded border border-gray-200 bg-gray-200 focus:bg-white focus:outline-none focus:ring-2 focus:ring-yellow-600 focus:border-transparent"
                placeholder="{{t('Search...')}}" />
            <svg class="w-4 h-4 absolute left-2.5 top-3.5" xmlns="http://www.w3.org/2000/svg" fill="none"
                viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                    d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
            </svg>
        </div>
        <div id="slepjams">
            <h3 class="mt-2 text-sm">{{t('Found users: ')}}</h3>
            <ul id="useriem" class="bg-white border border-gray-100 w-full mt-2 ">

            </ul>
            <h3 class="mt-2 text-sm">{{t('Found groups: ')}}</h3>
            <ul id="grupam" class="bg-white border border-gray-100 w-full mt-2 ">

            </ul>
            <div>
            </div>
            <li id="usersLi"
                class="pl-2 pr-2 py-1 border-b-2 border-gray-100 relative cursor-pointer hover:bg-yellow-50 hover:text-gray-900">
            </li>
            <li id="grupasLi"
                class="pl-2 pr-2 py-1 border-b-2 border-gray-100 relative cursor-pointer hover:bg-yellow-50 hover:text-gray-900">
            </li>
        </div>
        <script type="text/javascript">
            $('#searchUsersAndGroups').on('keyup',function(){
                    //console.log($("#grupasLi").css('display'));
                $value=$(this).val();
                var url = "{{ route('event.search.usersAndGroups') }}";
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                type : "POST",
                url : url,
                data:{'notikumsId':{{$notikums->id}},'search':$value, _token: CSRF_TOKEN},
                success:function(data){
                    PadaritRedzamu();
                    PievienotUsers(data['Users']);
                    PievienotGrupas(data['Grupas']);
                },
                error: function (data) {
                  console.log('Error:', data);
                }
                });
                })
                function PadaritRedzamu()
                {
                    $("#slepjams").css('display', 'block');
                }
                function PievienotUsers(users)
                {
                    $("#useriem").empty();
                    for (let i in users)
                    {
                        //console.log(i);
                        //console.log('Id: ' + users[i].id + ' Name: ' + users[i].name);
                        var li = $("#usersLi").clone(true, true);
                        li.attr('id', null);
                        li.css('display', 'list-item');
                        li.addClass('usersLi');
                        li.attr('user-id', users[i].id);
                        //console.log(li);
                        li.text(users[i].name);
                        $("#useriem").append(li);
                    }
                }
                function PievienotGrupas(grupas)
                {
                    $("#grupam").empty();
                    //console.log(grupas);
                    for (let i in grupas)
                    {
                        var li = $("#grupasLi").clone(true, true);
                        li.attr('id', null);
                        li.css('display', 'list-item');
                        li.addClass('grupasLi');
                        li.attr('grupa-id', grupas[i].id);
                        //console.log(li);
                        li.text(grupas[i].nosaukums);
                        $("#grupam").append(li);
                    }
                }
                $('#useriem').on('click', '.usersLi',function(){
                    //console.log(this);
                    var li = $(this);
                    var url = "{{ route('event.add.user') }}";
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                    $.ajax({
                    type : "POST",
                    url : url,
                    data:{'notikumsId':{{$notikums->id}},'userId':$(this).attr('user-id'), _token: CSRF_TOKEN},
                    success:function(data){
                    addUserToList(data.user);
                    li.remove();
                },
                error: function (data) {
                  console.log('Error:', data);
                }
                });
                });
                $('#grupam').on('click', '.grupasLi',function(){
                    //console.log(this);
                    var li = $(this);
                    var url = "{{ route('event.add.group') }}";
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                    $.ajax({
                    type : "POST",
                    url : url,
                    data:{'notikumsId':{{$notikums->id}}, 'grupaId':$(this).attr('grupa-id'), _token: CSRF_TOKEN},
                    success:function(data){
                    addGrupaToList(data.grupaId, data.nosaukums);
                    addUserToList(data.Users)
                    li.remove();

                },
                error: function (data) {
                  console.log('Error:', data);
                }
                });
                });
        </script>
        <style>
            #slepjams {
                display: none;
            }

            #usersLi,
            #grupasLi {
                display: none;
                //Default list-item
            }
        </style>
    </div>
</div>
