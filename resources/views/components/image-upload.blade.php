<div class="upload-div">
    <p class="text-red-800 mt-2">{{t('If you want to change your profile picture then you need to click choose file and then upload. After that you can click save to update your profile!')}}</p>
    <div class="text-gray-800 mt-2" id="message" style="display: none"></div>
    <form method="POST" id="upload_form" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <table class="table">
                <tr>
                    <td class="text-gray-800 mt-2"><label>{{t('Select File for Upload')}}</label></td>
                    <td class="text-gray-800 mt-2"><input class="bg-gray-500 px-5 py-3 text-sm shadow-sm font-bold tracking-wider border text-white rounded-full hover:shadow-lg hover:bg-gray-600" type="file" name="select_file" id="select_file" /></td>
                    <td class="text-gray-800 mt-2">
                    <input type="submit" name="upload" id="upload" class="bg-green-500 px-5 py-3 text-sm shadow-sm font-bold tracking-wider border text-white rounded-full hover:shadow-lg hover:bg-green-600"
                            value="{{t('Upload')}}"></td>
                </tr>
                <tr>
                    <td></td>
                    <td ><span class="text-muted text-gray-800 mt-2">jpg, jpeg, png, gif</span></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </form>
    <br />
    <span id="uploaded_image"></span>
</div>

<script>
    $(document).ready(function(){
     $(document).on('submit', '#upload_form', function(event){
      console.log(event);
      event.preventDefault();
      $.ajax({
       url:"{{ route('ajaxUpload') }}",
       method:"POST",
       data:new FormData(this),
       dataType:'JSON',
       contentType: false,
       cache: false,
       processData: false,
       success:function(data)
       {
        $('#message').css('display', 'block');
        $('#message').html(data.message);
        $('#message').addClass(data.class_name);
        $('#uploaded_image').html('<img src="'+ data.uploaded_image +'" class="img-thumbnail" width="300" />');
        var input = $(document.createElement('input'));
        input.attr('hidden', 'hidden');
        input.attr('value', data.uploaded_image);
        input.attr('name', 'avatar_url');
        $('#userForm').append(input);
        console.log(data.uploaded_image);
       }
      })
     });
    });
</script>