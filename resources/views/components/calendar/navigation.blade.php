<div class="w-full text-center py-2">
    <x-year-month-picker 
    :selectedYear="$yearMonthPickerData['izveletaisGads']"
    :selectedMonth="$yearMonthPickerData['izveletaisMenesis']" 
    :minYear="$yearMonthPickerData['minYear']" 
    :maxYear="$yearMonthPickerData['maxYear']"
    prevUrl="{{route('calendar.view.prev')}}" 
    nextUrl="{{route('calendar.view.next')}}"
    postUrl="{{route('calendar.view')}}"   />
</div>