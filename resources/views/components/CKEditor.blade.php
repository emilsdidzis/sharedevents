@props(['value', 'name', 'title'])
<div class="mb-8">
    <label class="text-xl text-gray-600">{{$title}}</label><br/>
    <textarea name={{$name}} id="CKcontent" class="border-2 border-gray-500">
        {!! $value !!}
</textarea>
    <script src="/js/ckeditor.js"></script>
    <script>
        var editors=ClassicEditor.create(document.querySelector('#CKcontent'), {
    simpleUpload: {
        uploadUrl: {
        url: "{{route('CKEUpload')}}"
        }
    }
    }).then(editor => {
    console.log('Editor created successfully!');
    }).catch(err => {
    console.error(err.stack);
    });
    </script>
</div>