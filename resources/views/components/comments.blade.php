@props(['notikums'])
<div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200">
                <h1><strong>{{t('Comments')}}</strong></h1>
                <!-- comment form -->
                <div class="flex mx-auto items-center justify-center shadow-lg mt-4 mx-8 mb-4 max-w-lg">
                    <form class="w-full max-w-xl bg-white rounded-lg px-4 pt-2" method="POST"
                        action="{{route('comment.add', ['id' => $notikums->id])}}">
                        @csrf
                        <div class="flex flex-wrap -mx-3 mb-6">
                            <h2 class="px-4 pt-3 pb-2 text-gray-800 text-lg">{{t('Add a new comment')}}</h2>
                            <div class="w-full md:w-full px-3 mb-2 mt-2">
                                <x-validation-error class="mb-4" :errors="$errors" title="saturs" />
                                <textarea name="saturs"
                                    class="bg-gray-100 rounded border border-gray-400 leading-normal resize-none w-full h-20 py-2 px-3 font-medium placeholder-gray-700 focus:outline-none focus:bg-white"
                                    name="body" placeholder="{{t('Type Your Comment')}}" required></textarea>
                            </div>
                            <div class="w-full flex items-start md:w-full px-3">
                                <div class="flex items-start w-1/2 text-gray-700 px-2 mr-auto">
                                    <svg fill="none" class="w-5 h-5 text-gray-600 mr-1" viewBox="0 0 24 24"
                                        stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                                    </svg>
                                    <p class="text-xs md:text-sm pt-px">{{t('Max character count is 500')}}</p>
                                </div>
                                <div class="-mr-1">
                                    <input type='submit'
                                        class="bg-white text-gray-700 font-medium py-1 px-4 border border-gray-400 rounded-lg tracking-wide mr-1 hover:bg-gray-100"
                                        value="{{t('Post Comment')}}">
                                </div>
                            </div>
                    </form>
                </div>
            </div>
            <section class="rounded-b-lg  mt-4 ">
                <div id="task-comments flex flex-col" class="pt-4">
                    @foreach($notikums->Komentari()->orderBy('created_at', 'desc')->get() as $komentars)
                    <!--     comment-->
                    <div komentars-id-galvenais="{{$komentars->id}}"
                        class="bg-white rounded-lg p-3  flex flex-col justify-center items-center md:items-start shadow-lg mb-4">

		                <div class="flex flex-row justify-center mr-2">
                            <img alt="avatar" width="48" height="48" class="rounded-full w-10 h-10 mr-4 shadow-lg mb-4"
                                src="{{$komentars->Autors()->get()[0]->avatar_url==null ? 'https://cdn1.iconfinder.com/data/icons/technology-devices-2/100/Profile-512.png' : $komentars->Autors()->get()[0]->avatar_url}}">

                            <h3 class="text-purple-600 py-2 font-semibold text-lg text-center md:text-left ">
                                {{$komentars->Autors()->get()[0]->name}}
                            </h3>
                            @if ($komentars->TgdIrAutors()==true || auth()->user()->isAdmin())
                            <form method="POST" action="{{route('comment.delete', $komentars->id)}}">
                            @csrf
                            <button type="submit" onclick="event.preventDefault();this.closest('form').submit();"
                            class="btn-delete ml-2 h-10 px-4 py-2 bg-gray-500 shadow-lg border rounded-lg text-white uppercase font-semibold tracking-wider focus:outline-none focus:shadow-outline hover:bg-teal-400 active:bg-teal-400">
                            {{t('Delete')}}</button>
                            </form>
                            @endif
                        </div>


                        <p style="width: 90%" class="text-gray-600 text-lg text-center md:text-left "><span
                                class="text-purple-600 font-semibold">{{t('Posted at: ').$komentars->created_at}}</span>
                            <br />{{$komentars->saturs}}</p>

                    </div>
                    @endforeach
                    <!--  comment end-->
                </div>
            </section>
        </div>
    </div>
</div>
</div>
</div>
