
<div id="year-month-picker" class="flex-auto">
  <form class="inline-flex align-bottom" action="{{$prevUrl}}">
      <x-action-button class="mr-2">{{t('Previous month')}}</x-action-button>
      <input name="year" value="{{$selectedYear}}" hidden>
      <input name="month" value="{{$selectedMonth}}" hidden>
  </form>
  <form class="inline-flex" id="year-month-form" action="{{$postUrl}}">
    <div class="pr-1">
    <label class="block" for="year">{{t('Year')}}</label>
    <select class="block" name="year" id="year">
      @foreach($gadi as $gads)
        <option value="{{$gads}}" {{($gads==$selectedYear) ? "selected" : ""}}>{{$gads}}</option>
      @endforeach
    </select>
    </div>
    <div class="">
    <label class="block" for="month">{{t('Month')}}</label>
    <select class="block" name="month" id="month">
      @for ($i=1;$i<=12;$i++)
        <option value="{{$i}}" {{($i==$selectedMonth) ? "selected" : ""}}>{{$menesi[$i]}}</option>
      @endfor
    </select>
    </div>
  </form>
  <form class="inline-flex align-bottom items-start" action="{{$nextUrl}}">
      <x-action-button class="ml-2">{{t('Next month')}}</x-action-button>
      <input name="year" value="{{$selectedYear}}" hidden>
      <input name="month" value="{{$selectedMonth}}" hidden>
  </form>
</div>

<script>
  $(document).ready(function () {
    $("#month").on("input", function() {
   $("#year-month-form").submit();
    });
    $("#year").on("input", function() {
   $("#year-month-form").submit();
    });
  });
</script>