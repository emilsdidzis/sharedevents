@props(['grupa'])
@if (!$grupa->TgdBlokeja())
<div class="flex my-10" group-id-galvenais="{{$grupa->id}}">
  <div class="bg-white w-3/4 m-auto border-1  border-dashed border-gray-300 shadow-md rounded-lg overflow-hidden">
    <div class="p-4">
      <p class="mb-1 text-gray-900 font-semibold">{{$grupa->nosaukums}}</p>
      <p class="mb-1 text-gray-800 font-semibold">{{t("User's count: ").$grupa->GrupasUsers()->count()}}</p>

      <div class="mt-8 mb-3">
        @if ($grupa->TgdIrAutors()==true || auth()->user()->IsAdmin())
        <a href="{{url('group/edit/'.$grupa->id)}}"
          class="px-4 py-2 bg-gray-500 shadow-lg border rounded-lg text-white uppercase font-semibold tracking-wider focus:outline-none focus:shadow-outline hover:bg-teal-400 active:bg-teal-400">
          {{t('Edit')}}</a>
        <a href="" group-id="{{$grupa->id}}"
          class="btn-delete px-4 py-2 bg-gray-500 shadow-lg border rounded-lg text-white uppercase font-semibold tracking-wider focus:outline-none focus:shadow-outline hover:bg-teal-400 active:bg-teal-400">
          {{t('Delete')}}</a>
        @endif
        @if ($grupa->TgdIrAutors()==false)
        <form style="display:inline" method="POST" action="{{route('group.leave', [$grupa->id])}}">
          @csrf
          <input hidden name="id" value="{{$grupa->id}}" />
          <button role="submit"
            class="px-4 py-2 bg-gray-500 shadow-lg border rounded-lg text-white uppercase font-semibold tracking-wider focus:outline-none focus:shadow-outline hover:bg-teal-400 active:bg-teal-400">
            {{t('Leave')}}</a>
        </form>
        <form style="display:inline" method="POST" action="{{route('group.block')}}">
          @csrf
          <input hidden name="id" value="{{$grupa->id}}" />
          <button role="submit"
            class="px-4 py-2 bg-red-500 shadow-lg border rounded-lg text-white uppercase font-semibold tracking-wider focus:outline-none focus:shadow-outline hover:bg-teal-400 active:bg-teal-400">
            {{t('Block')}}</button>
        </form>
        @endif
        <a href="{{url('group/view/'.$grupa->id)}}"
          class="px-4 py-2 bg-gray-500 shadow-lg border rounded-lg text-white uppercase font-semibold tracking-wider focus:outline-none focus:shadow-outline hover:bg-teal-400 active:bg-teal-400">
          {{t('View')}}</a>
      </div>
    </div>
  </div>
</div>
@endif