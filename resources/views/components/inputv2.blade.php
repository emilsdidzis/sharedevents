@props(['title', 'name', 'value', 'readonly' => 'false', 'type' => 'text'])
<div class="mb-4">
    <label class="text-xl text-gray-600">{{$title}}
        @if ($readonly!='true')
        <span class="text-red-500">*</span>
        @endif
    </label><br/>
    @if ($readonly=='true')
    <span class="text-red-600" >
        <strong>{{ t('Contact administrator to change ').$name }}</strong>
        </span>
    @endif
    <input type="{{$type}}" class="border-2 border-gray-300 p-2 w-full" name="{{$name}}" id="{{$name}}" value="{{$value}}"
        {{$readonly=='true' ? 'readonly' : 'required'}}>
</div>