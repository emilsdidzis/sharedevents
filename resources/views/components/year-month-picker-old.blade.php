
<div id="year-month-picker" class="inline-flex items-center">
  <form action="{{$prevUrl}}">
      <x-action-button class="mr-2">{{t('Previous month')}}</x-action-button>
      <input name="yearMonth" value="{{$gadsMenesis}}" hidden>
  </form>
  <form id="year-month-form" action="{{$postUrl}}">
    <input type="month" id="year-month" name="yearMonth" value="{{$gadsMenesis}}">
  </form>
  <form action="{{$nextUrl}}">
      <x-action-button class="ml-2">{{t('Next month')}}</x-action-button>
      <input name="yearMonth" value="{{$gadsMenesis}}" hidden>
  </form>
</div>

<script>
  $(document).ready(function () {
    $("#year-month").on("input", function() {
   $("#year-month-form").submit();
});
  });
</script>