@props(['title', 'name', 'value', 'isEditable' => 1])
@if ($isEditable==1)
<label class="flex justify-start items-start">
    <div class="bg-white border-2 rounded border-gray-400 w-6 h-6 flex flex-shrink-0 justify-center items-center mr-2 focus-within:border-blue-500">

      <input name="{{$name}} "type="checkbox" class="checkbox opacity-0 absolute" {{$value == 1 ? 'checked' : ''}}>
      <svg class="fill-current hidden w-4 h-4 text-green-500 pointer-events-none" viewBox="0 0 20 20"><path d="M0 11l2-2 5 5L18 3l2 2L7 18z"/></svg>
    </div>
    <div class="select-none">{{$title}}</div>
  </label>
@else
<label class="flex justify-start items-start">
    <div class="bg-gray-300 border-2 rounded border-gray-400 w-6 h-6 flex flex-shrink-0 justify-center items-center mr-2 focus-within:border-blue-500">

      <input name="{{$name}} "type="checkbox" class="checkbox opacity-0 absolute" {{$value == 1 ? 'checked' : ''}} onclick="return false;" disabled="disabled" readonly="readonly">
      <svg class="fill-current hidden w-4 h-4 text-green-500 pointer-events-none" viewBox="0 0 20 20"><path d="M0 11l2-2 5 5L18 3l2 2L7 18z"/></svg>
    </div>
    <div class="select-none text-gray-500">{{$title}}</div>
  </label>
@endif
  <style>
    input:checked + svg {
        display: block;
    }
  </style>