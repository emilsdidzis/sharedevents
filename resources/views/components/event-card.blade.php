<div class="flex my-10" event-id-galvenais="{{$notikums->id}}">
  <div class="bg-white w-3/4 m-auto border-1  border-dashed border-gray-300 shadow-md rounded-lg overflow-hidden">
    <div class="p-4">
      <p class="mb-1 text-gray-900 font-semibold">{{$notikums->virsraksts}}</p>
      <p class="mb-1 text-gray-800 font-semibold">{{t('Time')}}: {{$notikums->sakums}} - {{$notikums->beigas}}</p>
      <div class="max-h-28 break-words overflow-hidden">
      <span class="text-gray-700">{!!$notikums->saturs!!}</span>
      </div>

      <div class="mt-8 mb-3">
        @foreach($buttons[$notikums->id] as $button)
        <div class="inline-flex">
            <x-event-button :button=$button />
        </div>
        @endforeach
      </div>
    </div>
  </div>
</div>
 