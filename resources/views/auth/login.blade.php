<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ t('Login') }}
        </h2>
    </x-slot>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </x-slot>

        <!-- Session Status -->
        <x-auth-session-status class="mb-4" :status="session('status')" />

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mt-4" :errors="$errors" />
        <div class="flex justify-center">
            <!-- Google -->
            <a class="m-0.5" href="{{ url('auth/google')}}">
                <x-button  type="button">
                    {{t('Log in with Google')}}
                </x-button>
                </a>
            <a class="m-0.5" href="{{ url('auth/facebook')}}">
            <x-button  type="button">
                {{t('Log in with Facebook')}}
            </x-button>
            </a>
        </div>
        <form method="POST" action="{{ route('login') }}">
            @csrf

            <!-- Email Address -->
            <div>
                <x-input-label for="email" :value="t('Email')" />

                <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />

                <x-input-error :messages="$errors->get('email')" class="mt-2" />
            </div>

            <!-- Password -->
            <div class="mt-4">
                <x-input-label for="password" value="{{t('Password')}}" />

                <x-input id="password" class="block mt-1 w-full"
                                type="password"
                                name="password"
                                required autocomplete="current-password" />

                <x-input-error :messages="$errors->get('password')" class="mt-2" />
            </div>

            <!-- Remember Me -->
            <div class="block mt-4">
                <label for="remember_me" class="inline-flex items-center">
                    <input id="remember_me" type="checkbox" class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="remember">
                    <span class="ml-2 text-sm text-gray-600">{{ t('Remember me') }}</span>
                </label>
            </div>

            <div class="flex items-center justify-end mt-4">
                @if (Route::has('password.request'))
                    <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                        {{ t('Forgot your password?') }}
                    </a>
                @endif
                <a href="{{route('register')}}">
                <x-primary-button class="ml-3" type="button">{{t('Register')}}</x-primary-button></a>
                <x-primary-button class="ml-3">
                    {{ t('Log in') }}
                </x-primary-button>
            </div>
        </form>
    </x-auth-card>
</x-app-layout>
