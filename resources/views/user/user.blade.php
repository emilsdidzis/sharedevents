<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ $user->name }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                    <div class="flex">
                        <!-- Start of component -->
                        <div class="bg-white p-5 tracking-wide">
                            <div id="header" class="flex">
                                <img alt="Avatar" class="object-contain w-56 rounded-md"
                                    src="{{$user->avatar_url==null ? 'https://cdn1.iconfinder.com/data/icons/technology-devices-2/100/Profile-512.png' : $user->avatar_url}}" />
                                <div id="body" class="flex flex-col ml-5">
                                    <h4 id="name" class="text-xl font-semibold mb-2">{{$user->name}}</h4>
                                    <p class="text-gray-800 mt-2">{{t('Email:').' '.$user->email}}</p>
                                    <p class="text-gray-800 mt-2">{{t('Joined:').' '.$user->created_at}}</p>
                                    @if ($user->id==auth()->user()->id || auth()->user()->isAdmin())
                                    <p class="text-gray-800 mt-2">{{t('Receive notifications before event starts in email:').' '.($user->sanemt_pazinojumus==1 ? t('Yes') : t('No'))}}</p>
                                    <p class="text-gray-800 mt-2">{{t('Send email about events:').' '.$user->pazinot_pirms.' '.t('minutes before start')}}</p>
                                    @endif
                                    <p class="text-gray-800 mt-2">{{t('Created events:').' '.$user->UserIzveidotieNotikumi()->get()->count()}}</p>
                                    <p class="text-gray-800 mt-2">{{t('Involved in').' '.$user->UseraNotikumi()->get()->count().' '.t('events')}}</p>
                                    <p class="text-gray-800 mt-2">{{t('Created groups:').' '.$user->UserIzveidotasGrupas()->get()->count()}}</p>
                                    <p class="text-gray-800 mt-2">{{t('Involved in').' '.$user->UseraGrupas()->get()->count().' '.t('Groups')}}</p>
                                    <p class="text-gray-800 mt-2">{{t('Comments:').' '.$user->Komentari()->get()->count()}}</p>
                                    <p class="text-gray-800 mt-2">{{t('Webex connected to account: '.(sizeof($user->WebexTokens()->get())>0 ? t('Yes') : t('No')))}}</p>
                                </div>
                            </div>
                        </div>
                        <!-- End of component -->
                    </div>
                    @if ($user->id==auth()->user()->id || auth()->user()->isAdmin())
                        <a href="{{url('user/edit/'.$user->id)}}"
                        class="px-4 py-2 bg-gray-500 shadow-lg border rounded-lg text-white uppercase font-semibold tracking-wider focus:outline-none focus:shadow-outline hover:bg-teal-400 active:bg-teal-400">
                        {{t('Edit')}}</a>
                        <form class="inline-block" method="POST" action="{{ route('user.delete', $user->id) }}" >
                        @csrf
                        <button type="submit" onclick="event.preventDefault();this.closest('form').submit();"
                        class="btn-delete px-4 py-2 bg-gray-500 shadow-lg border rounded-lg text-white uppercase font-semibold tracking-wider focus:outline-none focus:shadow-outline hover:bg-teal-400 active:bg-teal-400">
                        {{t('Delete account')}}</button>
                        </form>
                    @endif
                    @if ($user->id==auth()->user()->id)
                        <a href="{{route('webex.auth')}}"
                        class="px-4 py-2 bg-gray-500 shadow-lg border rounded-lg text-white uppercase font-semibold tracking-wider focus:outline-none focus:shadow-outline hover:bg-teal-400 active:bg-teal-400">
                        {{t('Connect webex')}}</a>
                    @endif
                    @if ($user->id!=auth()->user()->id)
                    @if ($user->TgdIrNoblokejis())
                    <form style="display:inline" method="POST" action="{{route('user.block')}}">
                        @csrf
                        <input hidden name="blokejamais" value="{{$user->id}}" />
                        <input hidden name="bloket" value="0" />
                        <button role="submit"
                          class="px-4 py-2 bg-red-500 shadow-lg border rounded-lg text-white uppercase font-semibold tracking-wider focus:outline-none focus:shadow-outline hover:bg-teal-400 active:bg-teal-400">
                          {{t('Unblock')}}</button>
                      </form>
                    @else
                    <form style="display:inline" method="POST" action="{{route('user.block')}}">
                        @csrf
                        <input hidden name="blokejamais" value="{{$user->id}}" />
                        <input hidden name="bloket" value="1" />
                        <button role="submit"
                          class="px-4 py-2 bg-red-500 shadow-lg border rounded-lg text-white uppercase font-semibold tracking-wider focus:outline-none focus:shadow-outline hover:bg-teal-400 active:bg-teal-400">
                          {{t('Block')}}</button>
                      </form>
                    @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
