<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ t('Edit profile') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form method="POST" action="{{route('user.store', ['id' => $user->id])}}" id="userForm">
                        @csrf
                        <x-validation-error class="mb-4" :errors="$errors" title="name"/>   
                        <x-inputv2 name="name" value="{{$user->name}}" title="{{t('Name')}}" />

                            
                        <x-validation-error class="mb-4" :errors="$errors" title="email"/>   
                        @if (auth()->user()->isAdmin())
                        <x-inputv2 name="email" value="{{$user->email}}" title="{{t('Email')}}" />
                        @else
                        <x-inputv2 name="email" value="{{$user->email}}" title="{{t('Email')}}" readonly="true"/>
                        @endif
                        <x-validation-error class="mb-4" :errors="$errors" title="pazinot_pirms"/>   
                        <x-inputv2 name="pazinot_pirms" type="number" value="{{$user->pazinot_pirms}}" title="{{t('Send email about event start before(minutes)')}}" />
                         <br/>
                         <x-checkbox title="{{t('Receive emails before event start')}}" name="sanemt_pazinojumus[]" value="{{$user->sanemt_pazinojumus}}" /> 
                        <br/>
                         <x-validation-error class="mb-4" :errors="$errors" title="is_admin"/>   
                         
                        
                            <x-checkbox title="{{t('Is admin')}}" name="is_admin[]" value="{{$user->is_admin}}" isEditable="{{auth()->user()->is_admin}}" /> 
                            
                            <br/>
                        <div class="flex p-1">
                            <button role="submit" class="mb-4 p-3 bg-blue-500 text-white hover:bg-blue-400"
                                required>{{t('Save')}}</button>
                        </div>
                    </form>
                    <x-image-upload/>
                </div>
            </div>
        </div>
    </div>

</x-app-layout>