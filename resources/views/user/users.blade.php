<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ t('Users') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div>
                        <h1><strong>{{t('Search for users')}}</strong></h1>
                        <div class="inline-flex flex-col justify-center relative text-gray-500">
                            <div class="relative">
                                <input type="text" id="searchUsers"
                                    class="p-2 pl-8 rounded border border-gray-200 bg-gray-200 focus:bg-white focus:outline-none focus:ring-2 focus:ring-yellow-600 focus:border-transparent"
                                    placeholder="{{t('Search...')}}"
                                    maxlength="255"    
                                />
                                <svg class="w-4 h-4 absolute left-2.5 top-3.5" xmlns="http://www.w3.org/2000/svg"
                                    fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                        d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
                                </svg>
                            </div>
                            <div id="slepjams">
                                <h3 class="mt-2 text-sm">{{t('Found users: ')}}</h3>
                                <ul id="useriem" class="bg-white border border-gray-100 w-full mt-2 ">

                                </ul>
                                <li id="usersLi"
                                    class="pl-2 pr-2 py-1 border-b-2 border-gray-100 relative cursor-pointer hover:bg-yellow-50 hover:text-gray-900">
                                </li>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $('#searchUsers').on('keyup',function(){
            $value=$(this).val();
            var url = "{{ route('user.search') }}";
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
            type : "POST",
            url : url,
            data:{'search':$value, _token: CSRF_TOKEN},
            success:function(data){
                PadaritRedzamu();
                PievienotUsers(data['Users']);
            },
            error: function (data) {
              console.log('Error:', data);
            }
            });
            })
            function PadaritRedzamu()
            {
                $("#slepjams").css('display', 'block');
            }
            function PievienotUsers(users)
            {
                $("#useriem").empty();
                for (let i in users)
                {
                    var a = document.createElement('a');
                    $(a).attr('href', '/user/view/'+users[i].id);
                    var li = $("#usersLi").clone(true, true);
                    li.attr('id', null);
                    li.css('display', 'list-item');
                    li.addClass('usersLi');
                    li.attr('user-id', users[i].id);
                    li.text(users[i].name);
                    $(a).append(li);
                    $("#useriem").append(a);
                }
            }
    </script>
    <style>
        #slepjams {
            display: none;
        }

        #usersLi,
        #grupasLi {
            display: none;
            //Default list-item
        }
    </style>
</x-app-layout>
