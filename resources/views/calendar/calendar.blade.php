<!-- https://tailwindcomponents.com/component/tailwind-css-tables -->
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ t('Calendar') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200 overflow-y-hidden overflow-x-auto">
                    <div class="kalendars w-full">
                        <x-calendar.navigation  :$yearMonthPickerData />
                        <table class="w-full bg-blue-100 md:table-fixed table-auto overflow-hidden">
                            <tr class="bg-blue-600 text-center">
                                @foreach($nedelasDienas as $diena)
                                <th
                                class="
                                w-1/7
                                min-w-kolonnai
                                text-lg
                                font-semibold
                                text-white
                                py-4
                                lg:py-7
                                px-3
                                lg:px-4
                                border-l
                                border-blue-900
                                "
                                >
                                {{$diena}}
                                </th>
                                @endforeach
                            </tr>
                            <tbody>
                            @foreach($menesis as $nedela)
                            <tr class="border-blue-900 border-2">
                                @foreach($nedela->dienas as $diena)
                                <td class="border-blue-900 border-2 align-top h-20 whitespace-nowrap overflow-hidden overflow-ellipsis">
                                    <div class="text-right">
                                    @if ($diena->aktualaisMenesis)
                                    <span class="text-gray-900 mr-0 h-2 w-full font-bold">{{$diena->nr}}</span>
                                    @else
                                    <span class="text-gray-400 font-bold">{{$diena->nr}}</span>
                                    @endif
                                    </div>
                                    <div class="text-left">
                                    @foreach($diena->notikumi as $notikums)
                                        @if ($notikums->id!=-1)
                                        <a href="{{route('event.view', $notikums->id)}}">
                                        @endif
                                            <div class="w-full h-5 mt-1" style="background-color: {{($notikums->krasa=='transparent' ? 'rgba(0,0,0,0)' : $notikums->krasa)}};">
                                                <span class="text-sm text-white font-bold align-top whitespace-nowrap overflow-hidden overflow-ellipsis">{{$notikums->virsraksts}}</span>
                                        </div>
                                        @if ($notikums->id!=-1)
                                        </a>
                                        @endif
                                    @endforeach
                                    </div>
                                </td>
                                @endforeach
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
<style>
    .w-1\/7
    {
        width:14.285714%
    }
    .max-w-1\/7
    {
        max-width: 14.285714%;
    }
    .min-w-kolonnai
    {
        min-width: 100px;
    }
</style>