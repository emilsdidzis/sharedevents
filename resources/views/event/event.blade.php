<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ $notikums->virsraksts }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    @if ($notikums->TgdIrAutors()==true || auth()->user()->IsAdmin())
                    <div class="inline-flex">
                        <a href="{{url('event/edit/'.$notikums->id)}}"
                        class="px-4 py-2 bg-gray-500 shadow-lg border rounded-lg text-white uppercase font-semibold tracking-wider focus:outline-none focus:shadow-outline hover:bg-teal-400 active:bg-teal-400">
                        {{t('Edit')}}</a>
                        <form method="POST" action="{{ route('event.delete', $notikums->id) }}" >
                        @csrf
                        <button type="submit" onclick="event.preventDefault();this.closest('form').submit();"
                        class="btn-delete px-4 py-2 bg-gray-500 shadow-lg border rounded-lg text-white uppercase font-semibold tracking-wider focus:outline-none focus:shadow-outline hover:bg-teal-400 active:bg-teal-400">
                        {{t('Delete')}}</button>
                        </form>
                    </div>
                    @endif
                    <p><strong>{{t('Author:')}} </strong>{{$notikums->Autors()->get()[0]->name}} </p>
                    <p><strong>{{t('Start: ')}}</strong>{{$notikums->sakums}}</p>
                    <p><strong>{{t('End: ')}}</strong>{{$notikums->beigas}}</p>
                    <br />
                    <div class="sidebar-box">
                        <p>
                        {!! $notikums->saturs !!}
                        </p>
                        <p class="read-more"><a href="#"
                                class="button bg-gray-500 px-5 py-3 text-sm shadow-sm font-medium tracking-wider border text-gray-100 rounded-full hover:shadow-lg hover:bg-gray-600">
                                {{t('Read More')}}
                            </a>
                        </p>
                    </div>
                    <br/>
                    @if (sizeof($notikums->WebexMeeting()->get())>0)
                        <p id="pieteikusiesList"><strong>{{t('Webex meeting URL:').' '}}</strong>
                            <span class="mr-4 text-blue-600 underline">
                                <a href="{{$notikums->WebexMeeting()->get()[0]->join_url}}">
                                    {{t('Click here')}}
                                </a>
                            </span>
                        </p>
                    @endif
                    @if ($notikums->var_pieteikties)
                    <div>
                        <p id="pieteikusiesList"><strong>{{t('Joined users:').' '}}</strong>
                            @foreach($notikums->NotikumaUsers()->where('pieteicies', '=', '1')->get() as $user)
                            <span class="user mr-4" user-id="{{$user->id}}">
                                <a href="{{route('user.view', [$user->id])}}">
                                    {{$user->name}}
                                </a>
                            </span>
                            @endforeach
                        </p>
                    </div>
                    @endif
                    <div>
                        <p id="useruList"><strong>{{t('Invited users:').' '}}</strong>
                            @foreach($notikums->NotikumaUsers()->get() as $user)
                            <span class="user mr-4" user-id="{{$user->id}}">
                                <a href="{{route('user.view', [$user->id])}}">
                                    {{$user->name}}
                                </a>
                                @if(($notikums->TgdIrAutors() || auth()->user()->isAdmin()) && $notikums->autora_id!=$user->id)
                                <svg xmlns="http://www.w3.org/2000/svg" class="remove-user h-5 w-5 inline"
                                    viewBox="0 0 20 20" fill="#c53030" user-id="{{$user->id}}">
                                    <path fill-rule="evenodd"
                                        d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                                        clip-rule="evenodd" />
                                </svg>
                                @endif
                            </span>
                            @endforeach
                        </p>
                    </div>
                    <div>
                        <p id="grupuList"><strong>{{t('Invited groups:').' '}}</strong>
                            @foreach($notikums->NotikumaGrupas()->get() as $grupa)
                            <span class="grupa mr-4" group-id="{{$grupa->id}}">
                                <a href="{{route('group.view', [$grupa->id])}}">
                                    {{$grupa->nosaukums}}
                                </a>
                                @if($notikums->TgdIrAutors() || auth()->user()->isAdmin())
                                <svg xmlns="http://www.w3.org/2000/svg" class="remove-group h-5 w-5 inline"
                                    viewBox="0 0 20 20" fill="#c53030" group-id="{{$grupa->id}}">
                                    <path fill-rule="evenodd"
                                        d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                                        clip-rule="evenodd" />
                                </svg>
                                @endif
                            </span>
                            @endforeach
                        </p>
                    </div>
                    <br />
                    @if($notikums->TgdIrAutors() || auth()->user()->isAdmin())
                    <x-searchUsersGroups :notikums=$notikums />
                    @endif
                </div>
            </div>
        </div>
    </div>
    <x-comments :notikums=$notikums />
    <style>
        .sidebar-box {
            max-height: 140px;
            position: relative;
            overflow: hidden;
        }

        .sidebar-box .read-more {
            position: absolute;
            bottom: 0;
            left: 0;
            width: 100%;
            text-align: center;
            margin: 0;
            padding: 30px 0;

            /* "transparent" only works here because == rgba(0,0,0,0) */
            background-image: linear-gradient(to bottom, transparent, rgb(255, 255, 255));
        }

        .remove-group:hover,
        .remove-user:hover {
            cursor: pointer;
        }
    </style>
    <script>
        $(document).ready(function(){
           
            $("#grupuList").on('click', '.remove-group', function(){
                var url = "{{ route('event.remove.group') }}";
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                var btn = $(this);
                var notikumsId=btn.attr('group-id');
                //console.log(btn.attr('group-id'));
                $.ajax({
                type: "POST",
                 url: url,
                data: {notikumsId: {{$notikums->id}},grupaId: btn.attr('group-id'), _token: CSRF_TOKEN},
                success: function (data) {

                  $('span[group-id="' + data['grupaId'] +'"]').remove();
                    for (let i in data['nonemtie'])
                    {
                        $('span[user-id="' + data['nonemtie'][i] +'"]').remove();
                    }
                    //console.log(data['nonemtie']);
                },
                error: function (data) {
                  console.log('Error:', data);
                }
                });
            });
            $("#useruList").on('click', '.remove-user', function(){
                var url = "{{ route('event.remove.user') }}";
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                var btn = $(this);
                var notikumsId=btn.attr('user-id');
                //console.log(btn.attr('user-id'));
                $.ajax({
                type: "POST",
                 url: url,
                data: {notikumsId: {{$notikums->id}},userId: btn.attr('user-id'), _token: CSRF_TOKEN},
                success: function (data) {

                  $('span[user-id="' + data['Nonemtais'] +'"]').remove();
                },
                error: function (data) {
                  console.log('Error:', data);
                }
                });
            });
        });
        function addUserToList(useri)
        {
            for (let i in useri)
            {
                //console.log(useri[i]);
                var elem= $.parseHTML($.trim(`<span class='user mr-4' >
                            <a>

                                </a>
                                @if($notikums->TgdIrAutors() || auth()->user()->isAdmin())
                                <svg xmlns='http://www.w3.org/2000/svg' class='remove-user h-5 w-5 inline'
                                    viewBox='0 0 20 20' fill='#c53030'>
                                    <path fill-rule='evenodd'
                                        d='M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z'
                                        clip-rule='evenodd' />
                                </svg>
                                @endif
                            </span>`));
                $(elem).attr('user-id', useri[i].id);
                $(elem).find("svg").attr('user-id', useri[i].id);
                $(elem).find("a").attr('href', '/user/view/' + useri[i].id);
                $(elem).find("a").text(useri[i].name);
                $("#useruList").append(elem);
            }
        }
        function addGrupaToList(id, nosaukums)
        {
            var elem = $.parseHTML($.trim( `<span class="grupa mr-4">
                                <a >

                                </a>
                                @if($notikums->TgdIrAutors() || auth()->user()->isAdmin())
                                <svg xmlns="http://www.w3.org/2000/svg" class="remove-group h-5 w-5 inline"
                                    viewBox="0 0 20 20" fill="#c53030">
                                    <path fill-rule="evenodd"
                                        d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                                        clip-rule="evenodd" />
                                </svg>
                                @endif
                            </span>`));
                $(elem).attr('group-id', id);
                $(elem).find("svg").attr('group-id', id);
                $(elem).find("a").attr('href', '/group/view/' + id);
                $(elem).find("a").text(nosaukums);
                $("#grupuList").append(elem);
        }
        $(window).on('load', function(){ 
            var box=$(".sidebar-box");
            if (box.height()<140)
            {
            $(".read-more").remove();
                //$(".read-more").css('background-image', 'none');
               // $(".read-more").css({'background-image':''});
            }
            var $el, $ps, $up, totalHeight;
            $(".sidebar-box .button").click(function() {

            totalHeight = 0

            $el = $(this);
            $p  = $el.parent();
            $up = $p.parent();
            //$ps = $up.find("p:not('.read-more')");
            $ps = $up.children();

            // measure how tall inside should be by adding together heights of all inside paragraphs (except read-more paragraph)
            $ps.each(function() {
                totalHeight += $(this).outerHeight();
            });
            $up
                .css({
                // Set height to prevent instant jumpdown when max height is removed
                "height": $up.height(),
                "max-height": 9999
                })
                .animate({
                "height": totalHeight
                });

            // fade out read-more
            $p.fadeOut();

            // prevent jump-down
            return false;

            });
        });
    </script>
</x-app-layout>
