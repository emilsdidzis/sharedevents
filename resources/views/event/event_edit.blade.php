<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ t('Edit event') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form method="POST" action="{{route('event.store', ['id' => $id])}}">
                        @csrf
                        <x-validation-error class="mb-4" :errors="$errors" title="virsraksts"/>
                        <x-inputv2 name="virsraksts" value="{{$notikums->virsraksts}}" title="{{t('Title')}}" />


                        <x-validation-error class="mb-4" :errors="$errors" title="saturs"/>
                        <x-CKEditor name="saturs" value="{{$notikums->saturs}}" title="{{t('Content')}}">
                        </x-CKEditor>

                        <x-validation-error class="mb-4" :errors="$errors" title="sakums"/>
                         <x-datetime-picker title="{{t('Start')}}" name="sakums" value="{{$notikums->sakums}}"/>

                        <x-validation-error class="mb-4" :errors="$errors" title="beigas"/>
                         <x-datetime-picker title="{{t('End')}}" name="beigas" value="{{$notikums->beigas}}"/>
                         <br/>
                         <x-validation-error class="mb-4" :errors="$errors" title="var_pieteikties"/>
                        <x-checkbox title="{{t('Possible to join and unjoin')}}" name="var_pieteikties[]" value="{{$notikums->var_pieteikties}}"/>
                        <br/>
                        <x-validation-error class="mb-4" :errors="$errors" title="create_webex_meeting"/>
                        <x-checkbox title="{{t('Create Webex meeting for event')}}" name="create_webex_meeting[]" 
                        value="{{$notikums->webex_meeting_id!=null ? '1' : '0'}}" isEditable="{{sizeof(auth()->user()->WebexTokens()->get())>0 ? '1' : '0'}}"/>
                        <a href="{{route('webex.auth')}}" class="inline-block text-blue-600 underline">{{t('Connect Webex to your account first if you want to create a meeting!')}}</a>
                        <br/>
                        <div class="flex p-1">
                            <button role="submit" class="mb-4 p-3 bg-blue-500 text-white hover:bg-blue-400"
                                required>{{t('Save')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


</x-app-layout>
