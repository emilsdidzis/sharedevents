<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ t('All events') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200" id="eventlist">
                    <a href="{{route('event.create')}}">
                    <button
                        class="ml-4 bg-blue-600 px-5 py-3 text-sm shadow-sm font-bold tracking-wider border text-white rounded-md hover:shadow-lg hover:bg-blue-700"
                        >
                        {{t('Create event')}}</button>
                    </a>
                    @foreach ( $notikumi as $notikums)
                    <x-event-card :notikums=$notikums :buttons=$buttons />
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
        checkEventCount();

        function checkEventCount() {
            if (!$('div[event-id-galvenais]').length) {
                $('#eventlist').append('{{t("There are no events!")}}');
            }
        }
        $(".btn-join").on('click', function (e) {
          var url = "{{ route('event.join') }}";
          var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
          var btn = $(this);
          var notikumsId=btn.attr('event-id');
          e.preventDefault();
          console.log(btn.attr('event-id'));
          $.ajax({
              type: "POST",
              url: url,
              data: {id: btn.attr('event-id'), _token: CSRF_TOKEN},
              success: function (data) {
                  if(data['Pieteicies']==1)
                  {
                    $("a[event-id='"+notikumsId+"'][tips='u']").css('display', 'inline');
                    $("a[event-id='"+notikumsId+"'][tips='j']").css('display', 'none');
                  }
                  else
                  {
                    $("a[event-id='"+notikumsId+"'][tips='u']").css('display', 'none');
                    $("a[event-id='"+notikumsId+"'][tips='j']").css('display', 'inline');
                  }
                  console.log(data);
              },
              error: function (data) {
                  console.log('Error:', data);
              }
          });
      })

      var uzspiestasPogas=new Array();
      $(".btn-delete").on('click', function (e) {
          var url = "{{ route('event.delete.ajax') }}";
          var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
          var btn = $(this);
          var notikumsId=btn.attr('event-id');
          e.preventDefault();
         if (uzspiestasPogas.includes(notikumsId))
        {
        return;
        }
        uzspiestasPogas.push(notikumsId);
          //console.log(btn.attr('event-id'));
          $.ajax({
              type: "POST",
              url: url,
              data: {id: btn.attr('event-id'), _token: CSRF_TOKEN},
              success: function (data) {

                  $('div[event-id-galvenais="' + data['Id'] +'"]').remove();

                  checkEventCount();
              },
              error: function (data) {
                  console.log('Error:', data);
              }
          });
      });
      $(".btn-leave").on('click', function (e) {
          var url = "{{ route('event.leave') }}";
          var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
          var btn = $(this);
          var notikumsId=btn.attr('event-id');
          e.preventDefault();
          console.log(btn.attr('event-id'));
          $.ajax({
              type: "POST",
              url: url,
              data: {id: btn.attr('event-id'), _token: CSRF_TOKEN},
              success: function (data) {

                  $('div[event-id-galvenais="' + data['Id'] +'"]').remove();

                  checkEventCount();
              },
              error: function (data) {
                  console.log('Error:', data);
              }
          });
      });
    });
    </script>
</x-app-layout>
