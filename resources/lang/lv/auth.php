<?php
return [
    'failed'   => 'Ievadītie piekļuves dati nav pareizi.',
    'password' => 'Ievadītā parole nav pareiza',
    'throttle' => 'Pārāk daudz pieteikšanās mēģinājumi. Lūdzu mēģiniet vēlreiz pēc :seconds sekundēm.',
];
