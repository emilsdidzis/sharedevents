<?php
return [
    'reset'     => 'Jūsu parole ir atiestatīta!',
    'sent'      => 'Mēs jums nosūtījām paroles atiestatīšanas saiti uz e-pastu!',
    'throttled' => 'Lūdzu, uzgaidiet, pirms mēģiniet vēlreiz!',
    'token'     => 'Šī paroles atiestatīšanas pilnvara nav derīga.',
    'user'      => 'Mēs nevaram atrast lietotāju ar šo e-pasta adresi.',
];
